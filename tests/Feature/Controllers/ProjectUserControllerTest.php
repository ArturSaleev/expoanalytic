<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use App\Models\ProjectUser;

use App\Models\Project;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProjectUserControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            User::factory()->create(['email' => 'admin@admin.com'])
        );

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_displays_index_view_with_project_users()
    {
        $projectUsers = ProjectUser::factory()
            ->count(5)
            ->create();

        $response = $this->get(route('project-users.index'));

        $response
            ->assertOk()
            ->assertViewIs('app.project_users.index')
            ->assertViewHas('projectUsers');
    }

    /**
     * @test
     */
    public function it_displays_create_view_for_project_user()
    {
        $response = $this->get(route('project-users.create'));

        $response->assertOk()->assertViewIs('app.project_users.create');
    }

    /**
     * @test
     */
    public function it_stores_the_project_user()
    {
        $data = ProjectUser::factory()
            ->make()
            ->toArray();

        $response = $this->post(route('project-users.store'), $data);

        $this->assertDatabaseHas('project_users', $data);

        $projectUser = ProjectUser::latest('id')->first();

        $response->assertRedirect(route('project-users.edit', $projectUser));
    }

    /**
     * @test
     */
    public function it_displays_show_view_for_project_user()
    {
        $projectUser = ProjectUser::factory()->create();

        $response = $this->get(route('project-users.show', $projectUser));

        $response
            ->assertOk()
            ->assertViewIs('app.project_users.show')
            ->assertViewHas('projectUser');
    }

    /**
     * @test
     */
    public function it_displays_edit_view_for_project_user()
    {
        $projectUser = ProjectUser::factory()->create();

        $response = $this->get(route('project-users.edit', $projectUser));

        $response
            ->assertOk()
            ->assertViewIs('app.project_users.edit')
            ->assertViewHas('projectUser');
    }

    /**
     * @test
     */
    public function it_updates_the_project_user()
    {
        $projectUser = ProjectUser::factory()->create();

        $user = User::factory()->create();
        $project = Project::factory()->create();

        $data = [
            'user_id' => $user->id,
            'project_id' => $project->id,
        ];

        $response = $this->put(
            route('project-users.update', $projectUser),
            $data
        );

        $data['id'] = $projectUser->id;

        $this->assertDatabaseHas('project_users', $data);

        $response->assertRedirect(route('project-users.edit', $projectUser));
    }

    /**
     * @test
     */
    public function it_deletes_the_project_user()
    {
        $projectUser = ProjectUser::factory()->create();

        $response = $this->delete(route('project-users.destroy', $projectUser));

        $response->assertRedirect(route('project-users.index'));

        $this->assertSoftDeleted($projectUser);
    }
}
