<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use App\Models\ProjectData;

use App\Models\Project;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProjectDataControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            User::factory()->create(['email' => 'admin@admin.com'])
        );

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    protected function castToJson($json)
    {
        if (is_array($json)) {
            $json = addslashes(json_encode($json));
        } elseif (is_null($json) || is_null(json_decode($json))) {
            throw new \Exception(
                'A valid JSON string was not provided for casting.'
            );
        }

        return \DB::raw("CAST('{$json}' AS JSON)");
    }

    /**
     * @test
     */
    public function it_displays_index_view_with_all_project_data()
    {
        $allProjectData = ProjectData::factory()
            ->count(5)
            ->create();

        $response = $this->get(route('all-project-data.index'));

        $response
            ->assertOk()
            ->assertViewIs('app.all_project_data.index')
            ->assertViewHas('allProjectData');
    }

    /**
     * @test
     */
    public function it_displays_create_view_for_project_data()
    {
        $response = $this->get(route('all-project-data.create'));

        $response->assertOk()->assertViewIs('app.all_project_data.create');
    }

    /**
     * @test
     */
    public function it_stores_the_project_data()
    {
        $data = ProjectData::factory()
            ->make()
            ->toArray();

        $data['basic_data'] = json_encode($data['basic_data']);
        $data['basic_user'] = json_encode($data['basic_user']);
        $data['screen_data'] = json_encode($data['screen_data']);
        $data['request_body'] = json_encode($data['request_body']);
        $data['response_data'] = json_encode($data['response_data']);

        $response = $this->post(route('all-project-data.store'), $data);

        $data['basic_data'] = $this->castToJson($data['basic_data']);
        $data['basic_user'] = $this->castToJson($data['basic_user']);
        $data['screen_data'] = $this->castToJson($data['screen_data']);
        $data['request_body'] = $this->castToJson($data['request_body']);
        $data['response_data'] = $this->castToJson($data['response_data']);

        $this->assertDatabaseHas('project_data', $data);

        $projectData = ProjectData::latest('id')->first();

        $response->assertRedirect(route('all-project-data.edit', $projectData));
    }

    /**
     * @test
     */
    public function it_displays_show_view_for_project_data()
    {
        $projectData = ProjectData::factory()->create();

        $response = $this->get(route('all-project-data.show', $projectData));

        $response
            ->assertOk()
            ->assertViewIs('app.all_project_data.show')
            ->assertViewHas('projectData');
    }

    /**
     * @test
     */
    public function it_displays_edit_view_for_project_data()
    {
        $projectData = ProjectData::factory()->create();

        $response = $this->get(route('all-project-data.edit', $projectData));

        $response
            ->assertOk()
            ->assertViewIs('app.all_project_data.edit')
            ->assertViewHas('projectData');
    }

    /**
     * @test
     */
    public function it_updates_the_project_data()
    {
        $projectData = ProjectData::factory()->create();

        $project = Project::factory()->create();

        $data = [
            'session_id' => $this->faker->text(255),
            'basic_data' => [],
            'basic_user' => [],
            'visor_image' => $this->faker->text,
            'screen_data' => [],
            'request_method' => $this->faker->text(255),
            'request_url' => $this->faker->text(255),
            'request_body' => [],
            'response_url' => $this->faker->text(255),
            'response_data' => [],
            'date_time' => $this->faker->dateTime,
            'project_id' => $project->id,
        ];

        $data['basic_data'] = json_encode($data['basic_data']);
        $data['basic_user'] = json_encode($data['basic_user']);
        $data['screen_data'] = json_encode($data['screen_data']);
        $data['request_body'] = json_encode($data['request_body']);
        $data['response_data'] = json_encode($data['response_data']);

        $response = $this->put(
            route('all-project-data.update', $projectData),
            $data
        );

        $data['id'] = $projectData->id;

        $data['basic_data'] = $this->castToJson($data['basic_data']);
        $data['basic_user'] = $this->castToJson($data['basic_user']);
        $data['screen_data'] = $this->castToJson($data['screen_data']);
        $data['request_body'] = $this->castToJson($data['request_body']);
        $data['response_data'] = $this->castToJson($data['response_data']);

        $this->assertDatabaseHas('project_data', $data);

        $response->assertRedirect(route('all-project-data.edit', $projectData));
    }

    /**
     * @test
     */
    public function it_deletes_the_project_data()
    {
        $projectData = ProjectData::factory()->create();

        $response = $this->delete(
            route('all-project-data.destroy', $projectData)
        );

        $response->assertRedirect(route('all-project-data.index'));

        $this->assertModelMissing($projectData);
    }
}
