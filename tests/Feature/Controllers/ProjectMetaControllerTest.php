<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use App\Models\ProjectMeta;

use App\Models\Project;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProjectMetaControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            User::factory()->create(['email' => 'admin@admin.com'])
        );

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_displays_index_view_with_project_metas()
    {
        $projectMetas = ProjectMeta::factory()
            ->count(5)
            ->create();

        $response = $this->get(route('project-metas.index'));

        $response
            ->assertOk()
            ->assertViewIs('app.project_metas.index')
            ->assertViewHas('projectMetas');
    }

    /**
     * @test
     */
    public function it_displays_create_view_for_project_meta()
    {
        $response = $this->get(route('project-metas.create'));

        $response->assertOk()->assertViewIs('app.project_metas.create');
    }

    /**
     * @test
     */
    public function it_stores_the_project_meta()
    {
        $data = ProjectMeta::factory()
            ->make()
            ->toArray();

        $response = $this->post(route('project-metas.store'), $data);

        $this->assertDatabaseHas('project_metas', $data);

        $projectMeta = ProjectMeta::latest('id')->first();

        $response->assertRedirect(route('project-metas.edit', $projectMeta));
    }

    /**
     * @test
     */
    public function it_displays_show_view_for_project_meta()
    {
        $projectMeta = ProjectMeta::factory()->create();

        $response = $this->get(route('project-metas.show', $projectMeta));

        $response
            ->assertOk()
            ->assertViewIs('app.project_metas.show')
            ->assertViewHas('projectMeta');
    }

    /**
     * @test
     */
    public function it_displays_edit_view_for_project_meta()
    {
        $projectMeta = ProjectMeta::factory()->create();

        $response = $this->get(route('project-metas.edit', $projectMeta));

        $response
            ->assertOk()
            ->assertViewIs('app.project_metas.edit')
            ->assertViewHas('projectMeta');
    }

    /**
     * @test
     */
    public function it_updates_the_project_meta()
    {
        $projectMeta = ProjectMeta::factory()->create();

        $project = Project::factory()->create();

        $data = [
            'name' => $this->faker->name,
            'params' => $this->faker->text,
            'chart_name' => $this->faker->text(255),
            'on_main' => $this->faker->boolean,
            'project_id' => $project->id,
        ];

        $response = $this->put(
            route('project-metas.update', $projectMeta),
            $data
        );

        $data['id'] = $projectMeta->id;

        $this->assertDatabaseHas('project_metas', $data);

        $response->assertRedirect(route('project-metas.edit', $projectMeta));
    }

    /**
     * @test
     */
    public function it_deletes_the_project_meta()
    {
        $projectMeta = ProjectMeta::factory()->create();

        $response = $this->delete(route('project-metas.destroy', $projectMeta));

        $response->assertRedirect(route('project-metas.index'));

        $this->assertModelMissing($projectMeta);
    }
}
