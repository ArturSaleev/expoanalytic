<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use App\Models\ProjectLoadFile;

use App\Models\Project;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProjectLoadFileControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            User::factory()->create(['email' => 'admin@admin.com'])
        );

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_displays_index_view_with_project_load_files()
    {
        $projectLoadFiles = ProjectLoadFile::factory()
            ->count(5)
            ->create();

        $response = $this->get(route('project-load-files.index'));

        $response
            ->assertOk()
            ->assertViewIs('app.project_load_files.index')
            ->assertViewHas('projectLoadFiles');
    }

    /**
     * @test
     */
    public function it_displays_create_view_for_project_load_file()
    {
        $response = $this->get(route('project-load-files.create'));

        $response->assertOk()->assertViewIs('app.project_load_files.create');
    }

    /**
     * @test
     */
    public function it_stores_the_project_load_file()
    {
        $data = ProjectLoadFile::factory()
            ->make()
            ->toArray();

        $response = $this->post(route('project-load-files.store'), $data);

        $this->assertDatabaseHas('project_load_files', $data);

        $projectLoadFile = ProjectLoadFile::latest('id')->first();

        $response->assertRedirect(
            route('project-load-files.edit', $projectLoadFile)
        );
    }

    /**
     * @test
     */
    public function it_displays_show_view_for_project_load_file()
    {
        $projectLoadFile = ProjectLoadFile::factory()->create();

        $response = $this->get(
            route('project-load-files.show', $projectLoadFile)
        );

        $response
            ->assertOk()
            ->assertViewIs('app.project_load_files.show')
            ->assertViewHas('projectLoadFile');
    }

    /**
     * @test
     */
    public function it_displays_edit_view_for_project_load_file()
    {
        $projectLoadFile = ProjectLoadFile::factory()->create();

        $response = $this->get(
            route('project-load-files.edit', $projectLoadFile)
        );

        $response
            ->assertOk()
            ->assertViewIs('app.project_load_files.edit')
            ->assertViewHas('projectLoadFile');
    }

    /**
     * @test
     */
    public function it_updates_the_project_load_file()
    {
        $projectLoadFile = ProjectLoadFile::factory()->create();

        $project = Project::factory()->create();

        $data = [
            'project_id' => $project->id,
        ];

        $response = $this->put(
            route('project-load-files.update', $projectLoadFile),
            $data
        );

        $data['id'] = $projectLoadFile->id;

        $this->assertDatabaseHas('project_load_files', $data);

        $response->assertRedirect(
            route('project-load-files.edit', $projectLoadFile)
        );
    }

    /**
     * @test
     */
    public function it_deletes_the_project_load_file()
    {
        $projectLoadFile = ProjectLoadFile::factory()->create();

        $response = $this->delete(
            route('project-load-files.destroy', $projectLoadFile)
        );

        $response->assertRedirect(route('project-load-files.index'));

        $this->assertModelMissing($projectLoadFile);
    }
}
