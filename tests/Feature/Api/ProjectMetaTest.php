<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\ProjectMeta;

use App\Models\Project;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProjectMetaTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_project_metas_list()
    {
        $projectMetas = ProjectMeta::factory()
            ->count(5)
            ->create();

        $response = $this->getJson(route('api.project-metas.index'));

        $response->assertOk()->assertSee($projectMetas[0]->name);
    }

    /**
     * @test
     */
    public function it_stores_the_project_meta()
    {
        $data = ProjectMeta::factory()
            ->make()
            ->toArray();

        $response = $this->postJson(route('api.project-metas.store'), $data);

        $this->assertDatabaseHas('project_metas', $data);

        $response->assertStatus(201)->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_updates_the_project_meta()
    {
        $projectMeta = ProjectMeta::factory()->create();

        $project = Project::factory()->create();

        $data = [
            'name' => $this->faker->name,
            'params' => $this->faker->text,
            'chart_name' => $this->faker->text(255),
            'on_main' => $this->faker->boolean,
            'project_id' => $project->id,
        ];

        $response = $this->putJson(
            route('api.project-metas.update', $projectMeta),
            $data
        );

        $data['id'] = $projectMeta->id;

        $this->assertDatabaseHas('project_metas', $data);

        $response->assertOk()->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_deletes_the_project_meta()
    {
        $projectMeta = ProjectMeta::factory()->create();

        $response = $this->deleteJson(
            route('api.project-metas.destroy', $projectMeta)
        );

        $this->assertModelMissing($projectMeta);

        $response->assertNoContent();
    }
}
