<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\ProjectUser;

use App\Models\Project;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProjectUserTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_project_users_list()
    {
        $projectUsers = ProjectUser::factory()
            ->count(5)
            ->create();

        $response = $this->getJson(route('api.project-users.index'));

        $response->assertOk()->assertSee($projectUsers[0]->id);
    }

    /**
     * @test
     */
    public function it_stores_the_project_user()
    {
        $data = ProjectUser::factory()
            ->make()
            ->toArray();

        $response = $this->postJson(route('api.project-users.store'), $data);

        $this->assertDatabaseHas('project_users', $data);

        $response->assertStatus(201)->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_updates_the_project_user()
    {
        $projectUser = ProjectUser::factory()->create();

        $user = User::factory()->create();
        $project = Project::factory()->create();

        $data = [
            'user_id' => $user->id,
            'project_id' => $project->id,
        ];

        $response = $this->putJson(
            route('api.project-users.update', $projectUser),
            $data
        );

        $data['id'] = $projectUser->id;

        $this->assertDatabaseHas('project_users', $data);

        $response->assertOk()->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_deletes_the_project_user()
    {
        $projectUser = ProjectUser::factory()->create();

        $response = $this->deleteJson(
            route('api.project-users.destroy', $projectUser)
        );

        $this->assertSoftDeleted($projectUser);

        $response->assertNoContent();
    }
}
