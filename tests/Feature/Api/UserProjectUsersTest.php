<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\ProjectUser;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserProjectUsersTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_user_project_users()
    {
        $user = User::factory()->create();
        $projectUsers = ProjectUser::factory()
            ->count(2)
            ->create([
                'user_id' => $user->id,
            ]);

        $response = $this->getJson(
            route('api.users.project-users.index', $user)
        );

        $response->assertOk()->assertSee($projectUsers[0]->id);
    }

    /**
     * @test
     */
    public function it_stores_the_user_project_users()
    {
        $user = User::factory()->create();
        $data = ProjectUser::factory()
            ->make([
                'user_id' => $user->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route('api.users.project-users.store', $user),
            $data
        );

        $this->assertDatabaseHas('project_users', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $projectUser = ProjectUser::latest('id')->first();

        $this->assertEquals($user->id, $projectUser->user_id);
    }
}
