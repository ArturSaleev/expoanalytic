<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\Project;
use App\Models\ProjectUser;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProjectProjectUsersTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_project_project_users()
    {
        $project = Project::factory()->create();
        $projectUsers = ProjectUser::factory()
            ->count(2)
            ->create([
                'project_id' => $project->id,
            ]);

        $response = $this->getJson(
            route('api.projects.project-users.index', $project)
        );

        $response->assertOk()->assertSee($projectUsers[0]->id);
    }

    /**
     * @test
     */
    public function it_stores_the_project_project_users()
    {
        $project = Project::factory()->create();
        $data = ProjectUser::factory()
            ->make([
                'project_id' => $project->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route('api.projects.project-users.store', $project),
            $data
        );

        $this->assertDatabaseHas('project_users', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $projectUser = ProjectUser::latest('id')->first();

        $this->assertEquals($project->id, $projectUser->project_id);
    }
}
