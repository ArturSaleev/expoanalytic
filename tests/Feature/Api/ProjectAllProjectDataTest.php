<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\Project;
use App\Models\ProjectData;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProjectAllProjectDataTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_project_all_project_data()
    {
        $project = Project::factory()->create();
        $allProjectData = ProjectData::factory()
            ->count(2)
            ->create([
                'project_id' => $project->id,
            ]);

        $response = $this->getJson(
            route('api.projects.all-project-data.index', $project)
        );

        $response->assertOk()->assertSee($allProjectData[0]->session_id);
    }

    /**
     * @test
     */
    public function it_stores_the_project_all_project_data()
    {
        $project = Project::factory()->create();
        $data = ProjectData::factory()
            ->make([
                'project_id' => $project->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route('api.projects.all-project-data.store', $project),
            $data
        );

        $this->assertDatabaseHas('project_data', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $projectData = ProjectData::latest('id')->first();

        $this->assertEquals($project->id, $projectData->project_id);
    }
}
