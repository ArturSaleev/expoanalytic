<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\ProjectData;

use App\Models\Project;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProjectDataTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_all_project_data_list()
    {
        $allProjectData = ProjectData::factory()
            ->count(5)
            ->create();

        $response = $this->getJson(route('api.all-project-data.index'));

        $response->assertOk()->assertSee($allProjectData[0]->session_id);
    }

    /**
     * @test
     */
    public function it_stores_the_project_data()
    {
        $data = ProjectData::factory()
            ->make()
            ->toArray();

        $response = $this->postJson(route('api.all-project-data.store'), $data);

        $this->assertDatabaseHas('project_data', $data);

        $response->assertStatus(201)->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_updates_the_project_data()
    {
        $projectData = ProjectData::factory()->create();

        $project = Project::factory()->create();

        $data = [
            'session_id' => $this->faker->text(255),
            'basic_data' => [],
            'basic_user' => [],
            'visor_image' => $this->faker->text,
            'screen_data' => [],
            'request_method' => $this->faker->text(255),
            'request_url' => $this->faker->text(255),
            'request_body' => [],
            'response_url' => $this->faker->text(255),
            'response_data' => [],
            'date_time' => $this->faker->dateTime,
            'project_id' => $project->id,
        ];

        $response = $this->putJson(
            route('api.all-project-data.update', $projectData),
            $data
        );

        $data['id'] = $projectData->id;

        $this->assertDatabaseHas('project_data', $data);

        $response->assertOk()->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_deletes_the_project_data()
    {
        $projectData = ProjectData::factory()->create();

        $response = $this->deleteJson(
            route('api.all-project-data.destroy', $projectData)
        );

        $this->assertModelMissing($projectData);

        $response->assertNoContent();
    }
}
