<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\Project;
use App\Models\ProjectLoadFile;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProjectProjectLoadFilesTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_project_project_load_files()
    {
        $project = Project::factory()->create();
        $projectLoadFiles = ProjectLoadFile::factory()
            ->count(2)
            ->create([
                'project_id' => $project->id,
            ]);

        $response = $this->getJson(
            route('api.projects.project-load-files.index', $project)
        );

        $response->assertOk()->assertSee($projectLoadFiles[0]->filename);
    }

    /**
     * @test
     */
    public function it_stores_the_project_project_load_files()
    {
        $project = Project::factory()->create();
        $data = ProjectLoadFile::factory()
            ->make([
                'project_id' => $project->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route('api.projects.project-load-files.store', $project),
            $data
        );

        $this->assertDatabaseHas('project_load_files', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $projectLoadFile = ProjectLoadFile::latest('id')->first();

        $this->assertEquals($project->id, $projectLoadFile->project_id);
    }
}
