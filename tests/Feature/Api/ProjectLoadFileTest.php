<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\ProjectLoadFile;

use App\Models\Project;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProjectLoadFileTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_project_load_files_list()
    {
        $projectLoadFiles = ProjectLoadFile::factory()
            ->count(5)
            ->create();

        $response = $this->getJson(route('api.project-load-files.index'));

        $response->assertOk()->assertSee($projectLoadFiles[0]->filename);
    }

    /**
     * @test
     */
    public function it_stores_the_project_load_file()
    {
        $data = ProjectLoadFile::factory()
            ->make()
            ->toArray();

        $response = $this->postJson(
            route('api.project-load-files.store'),
            $data
        );

        $this->assertDatabaseHas('project_load_files', $data);

        $response->assertStatus(201)->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_updates_the_project_load_file()
    {
        $projectLoadFile = ProjectLoadFile::factory()->create();

        $project = Project::factory()->create();

        $data = [
            'project_id' => $project->id,
        ];

        $response = $this->putJson(
            route('api.project-load-files.update', $projectLoadFile),
            $data
        );

        $data['id'] = $projectLoadFile->id;

        $this->assertDatabaseHas('project_load_files', $data);

        $response->assertOk()->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_deletes_the_project_load_file()
    {
        $projectLoadFile = ProjectLoadFile::factory()->create();

        $response = $this->deleteJson(
            route('api.project-load-files.destroy', $projectLoadFile)
        );

        $this->assertModelMissing($projectLoadFile);

        $response->assertNoContent();
    }
}
