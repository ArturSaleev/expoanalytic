<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\Project;
use App\Models\ProjectMeta;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProjectProjectMetasTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_project_project_metas()
    {
        $project = Project::factory()->create();
        $projectMetas = ProjectMeta::factory()
            ->count(2)
            ->create([
                'project_id' => $project->id,
            ]);

        $response = $this->getJson(
            route('api.projects.project-metas.index', $project)
        );

        $response->assertOk()->assertSee($projectMetas[0]->name);
    }

    /**
     * @test
     */
    public function it_stores_the_project_project_metas()
    {
        $project = Project::factory()->create();
        $data = ProjectMeta::factory()
            ->make([
                'project_id' => $project->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route('api.projects.project-metas.store', $project),
            $data
        );

        $this->assertDatabaseHas('project_metas', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $projectMeta = ProjectMeta::latest('id')->first();

        $this->assertEquals($project->id, $projectMeta->project_id);
    }
}
