<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property integer $id
 * @property integer $project_id
 * @property integer $phone_model_id
 * @property string $session_id
 * @property string $created_at
 * @property string $updated_at
 * @property ModelPhone $modelPhone
 * @property Project $project
 */
class ProjectModelPhone extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'project_model_phone';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['project_id', 'phone_model_id', 'session_id'];

    /**
     * @return BelongsTo
     */
    public function modelPhone()
    {
        return $this->belongsTo('App\Models\ModelPhone', 'phone_model_id');
    }

    /**
     * @return BelongsTo
     */
    public function project()
    {
        return $this->belongsTo('App\Models\Project');
    }
}
