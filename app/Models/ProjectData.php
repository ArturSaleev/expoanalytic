<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ProjectData extends Model
{
    use HasFactory;
    use Searchable;

    protected $fillable = [
        'project_id',
        'session_id',
        'basic_data',
        'basic_user',
        'visor_image',
        'screen_data',
        'request_method',
        'request_url',
        'request_body',
        'response_url',
        'response_data',
        'date_time',
    ];

    protected $searchableFields = ['*'];

    protected $table = 'project_data';

    protected $casts = [
        'basic_data' => 'array',
        'basic_user' => 'array',
        'screen_data' => 'array',
        'request_body' => 'array',
        'response_data' => 'array',
        'date_time' => 'datetime:d.m.Y H:i:s',
    ];

    public function project()
    {
        return $this->belongsTo(Project::class);
    }
}
