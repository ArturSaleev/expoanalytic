<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ProjectLoadFile extends Model
{
    use HasFactory;
    use Searchable;

    protected $fillable = ['project_id', 'filename'];

    protected $searchableFields = ['*'];

    protected $table = 'project_load_files';

    public function project()
    {
        return $this->belongsTo(Project::class);
    }
}
