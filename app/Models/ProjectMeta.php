<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ProjectMeta extends Model
{
    use HasFactory;
    use Searchable;

    protected $fillable = [
        'project_id',
        'name',
        'params',
        'chart_name',
        'on_main',
    ];

    protected $searchableFields = ['*'];

    protected $table = 'project_metas';

    protected $casts = [
        'on_main' => 'boolean',
    ];

    public function project()
    {
        return $this->belongsTo(Project::class);
    }
}
