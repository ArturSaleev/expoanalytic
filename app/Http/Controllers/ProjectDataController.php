<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\ProjectData;
use Illuminate\Http\Request;
use App\Http\Requests\ProjectDataStoreRequest;
use App\Http\Requests\ProjectDataUpdateRequest;

class ProjectDataController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', ProjectData::class);

        $search = $request->get('search', '');

        $allProjectData = ProjectData::search($search)
            ->latest()
            ->paginate(5)
            ->withQueryString();

        return view(
            'app.all_project_data.index',
            compact('allProjectData', 'search')
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', ProjectData::class);

        $projects = Project::pluck('name', 'id');

        return view('app.all_project_data.create', compact('projects'));
    }

    /**
     * @param \App\Http\Requests\ProjectDataStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProjectDataStoreRequest $request)
    {
        $this->authorize('create', ProjectData::class);

        $validated = $request->validated();
        $validated['basic_data'] = json_decode($validated['basic_data'], true);

        $validated['basic_user'] = json_decode($validated['basic_user'], true);

        $validated['screen_data'] = json_decode(
            $validated['screen_data'],
            true
        );

        $validated['request_body'] = json_decode(
            $validated['request_body'],
            true
        );

        $validated['response_data'] = json_decode(
            $validated['response_data'],
            true
        );

        $projectData = ProjectData::create($validated);

        return redirect()
            ->route('all-project-data.edit', $projectData)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\ProjectData $projectData
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, ProjectData $projectData)
    {
        $this->authorize('view', $projectData);

        return view('app.all_project_data.show', compact('projectData'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\ProjectData $projectData
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, ProjectData $projectData)
    {
        $this->authorize('update', $projectData);

        $projects = Project::pluck('name', 'id');

        return view(
            'app.all_project_data.edit',
            compact('projectData', 'projects')
        );
    }

    /**
     * @param \App\Http\Requests\ProjectDataUpdateRequest $request
     * @param \App\Models\ProjectData $projectData
     * @return \Illuminate\Http\Response
     */
    public function update(
        ProjectDataUpdateRequest $request,
        ProjectData $projectData
    ) {
        $this->authorize('update', $projectData);

        $validated = $request->validated();
        $validated['basic_data'] = json_decode($validated['basic_data'], true);

        $validated['basic_user'] = json_decode($validated['basic_user'], true);

        $validated['screen_data'] = json_decode(
            $validated['screen_data'],
            true
        );

        $validated['request_body'] = json_decode(
            $validated['request_body'],
            true
        );

        $validated['response_data'] = json_decode(
            $validated['response_data'],
            true
        );

        $projectData->update($validated);

        return redirect()
            ->route('all-project-data.edit', $projectData)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\ProjectData $projectData
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, ProjectData $projectData)
    {
        $this->authorize('delete', $projectData);

        $projectData->delete();

        return redirect()
            ->route('all-project-data.index')
            ->withSuccess(__('crud.common.removed'));
    }
}
