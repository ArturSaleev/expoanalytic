<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Project;
use App\Models\ProjectUser;
use Illuminate\Http\Request;
use App\Http\Requests\ProjectUserStoreRequest;
use App\Http\Requests\ProjectUserUpdateRequest;

class ProjectUserController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', ProjectUser::class);

        $search = $request->get('search', '');

        $projectUsers = ProjectUser::search($search)
            ->latest()
            ->paginate(5)
            ->withQueryString();

        return view(
            'app.project_users.index',
            compact('projectUsers', 'search')
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', ProjectUser::class);

        $users = User::pluck('name', 'id');
        $projects = Project::pluck('name', 'id');

        return view('app.project_users.create', compact('users', 'projects'));
    }

    /**
     * @param \App\Http\Requests\ProjectUserStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProjectUserStoreRequest $request)
    {
        $this->authorize('create', ProjectUser::class);

        $validated = $request->validated();

        $projectUser = ProjectUser::create($validated);

        return redirect()
            ->route('project-users.edit', $projectUser)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\ProjectUser $projectUser
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, ProjectUser $projectUser)
    {
        $this->authorize('view', $projectUser);

        return view('app.project_users.show', compact('projectUser'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\ProjectUser $projectUser
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, ProjectUser $projectUser)
    {
        $this->authorize('update', $projectUser);

        $users = User::pluck('name', 'id');
        $projects = Project::pluck('name', 'id');

        return view(
            'app.project_users.edit',
            compact('projectUser', 'users', 'projects')
        );
    }

    /**
     * @param \App\Http\Requests\ProjectUserUpdateRequest $request
     * @param \App\Models\ProjectUser $projectUser
     * @return \Illuminate\Http\Response
     */
    public function update(
        ProjectUserUpdateRequest $request,
        ProjectUser $projectUser
    ) {
        $this->authorize('update', $projectUser);

        $validated = $request->validated();

        $projectUser->update($validated);

        return redirect()
            ->route('project-users.edit', $projectUser)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\ProjectUser $projectUser
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, ProjectUser $projectUser)
    {
        $this->authorize('delete', $projectUser);

        $projectUser->delete();

        return redirect()
            ->route('project-users.index')
            ->withSuccess(__('crud.common.removed'));
    }
}
