<?php

namespace App\Http\Controllers\Api;

use App\Models\Project;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\ProjectUserResource;
use App\Http\Resources\ProjectUserCollection;

class ProjectProjectUsersController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Project $project
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Project $project)
    {
        $this->authorize('view', $project);

        $search = $request->get('search', '');

        $projectUsers = $project
            ->projectUsers()
            ->search($search)
            ->latest()
            ->paginate();

        return new ProjectUserCollection($projectUsers);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Project $project
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Project $project)
    {
        $this->authorize('create', ProjectUser::class);

        $validated = $request->validate([
            'user_id' => ['required', 'exists:users,id'],
        ]);

        $projectUser = $project->projectUsers()->create($validated);

        return new ProjectUserResource($projectUser);
    }
}
