<?php

namespace App\Http\Controllers\Api;

use App\Models\ProjectMeta;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\ProjectMetaResource;
use App\Http\Resources\ProjectMetaCollection;
use App\Http\Requests\ProjectMetaStoreRequest;
use App\Http\Requests\ProjectMetaUpdateRequest;

class ProjectMetaController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', ProjectMeta::class);

        $search = $request->get('search', '');

        $projectMetas = ProjectMeta::search($search)
            ->latest()
            ->paginate();

        return new ProjectMetaCollection($projectMetas);
    }

    /**
     * @param \App\Http\Requests\ProjectMetaStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProjectMetaStoreRequest $request)
    {
        $this->authorize('create', ProjectMeta::class);

        $validated = $request->validated();

        $projectMeta = ProjectMeta::create($validated);

        return new ProjectMetaResource($projectMeta);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\ProjectMeta $projectMeta
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, ProjectMeta $projectMeta)
    {
        $this->authorize('view', $projectMeta);

        return new ProjectMetaResource($projectMeta);
    }

    /**
     * @param \App\Http\Requests\ProjectMetaUpdateRequest $request
     * @param \App\Models\ProjectMeta $projectMeta
     * @return \Illuminate\Http\Response
     */
    public function update(
        ProjectMetaUpdateRequest $request,
        ProjectMeta $projectMeta
    ) {
        $this->authorize('update', $projectMeta);

        $validated = $request->validated();

        $projectMeta->update($validated);

        return new ProjectMetaResource($projectMeta);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\ProjectMeta $projectMeta
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, ProjectMeta $projectMeta)
    {
        $this->authorize('delete', $projectMeta);

        $projectMeta->delete();

        return response()->noContent();
    }
}
