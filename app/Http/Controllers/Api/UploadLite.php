<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\ModelPhone;
use App\Models\Project;
use App\Models\ProjectData;
use App\Models\ProjectLoadFile;
use App\Models\ProjectModelPhone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use PDO;

class UploadLite extends Controller
{
    public function load(Request $request)
    {
        $request->validate([
            'file' => 'required|max:5048',
            'project' => 'required',
        ]);

        $uid = $request->project;

        $project = Project::query()->where('uid', $uid)->first();
        if(!$project){
            return response()->json([
                "success" => false,
                "message" => "Project not found"
            ]);
        }
        if ($file = $request->file('file')) {
            $path = $file->store('public/files');

            $save = new ProjectLoadFile();
            $save->project_id = $project->id;
            $save->filename = $path;
            $save->save();

            $this->ParserDB($project->id, $path);

            return response()->json([
                "success" => true,
                "message" => "File successfully uploaded",
                "file" => $file
            ]);
        }else{
            return response()->json([
                "success" => false,
                "message" => "Error file"
            ]);
        }
    }

    private function ParserDB($project_id, $filename)
    {
        $file = public_path(Storage::url($filename));
        $db = new PDO("sqlite:$file");
        $basicTable = $db->query('SELECT * FROM BasicData');
        foreach($basicTable->fetchAll(mode: PDO::FETCH_ASSOC) as $row){

            $pd = ProjectData::query()->where([
                'session_id' => $row['session_id'],
                'project_id' => $project_id,
                'date_time' => $row['created_at']
            ])->first();
            if(!$pd){
                $pd = new ProjectData();
                $pd->project_id = $project_id;
                $pd->session_id = $row['session_id'];
            }

            $pd->basic_data = $row['value'];
            $pd->basic_user = $row['user'];
            $pd->date_time = date("Y-m-d H:i:s", strtotime($row['created_at']));
            $pd->save();

            if($row['value'] !== '') {
                $value = json_decode($row['value']);
                $modelPhone = ModelPhone::firstOrCreate(['name' => $value->deviceName]);
                ProjectModelPhone::firstOrCreate([
                    'project_id' => $project_id,
                    'session_id' => $row['session_id'],
                    'phone_model_id' => $modelPhone->id
                ]);
            }
        }

        $visorTable = $db->query('SELECT * FROM VisorData');
        foreach($visorTable->fetchAll(mode: PDO::FETCH_ASSOC) as $row){
            $pd = ProjectData::query()->where([
                'session_id' => $row['session_id'],
                'project_id' => $project_id,
                'date_time' => $row['created_at']
            ])->first();

            if(!$pd) {
                $pd = new ProjectData();
                $pd->project_id = $project_id;
                $pd->session_id = $row['session_id'];
            }
            $pd->visor_image = $row['value'];
            $pd->date_time = $row['created_at'];
            $pd->save();
        }

        $screenTable = $db->query('SELECT * FROM ScreenData');
        foreach($screenTable->fetchAll(mode: PDO::FETCH_ASSOC) as $row){
            $pd = ProjectData::query()->where([
                'session_id' => $row['session_id'],
                'project_id' => $project_id,
                'date_time' => $row['created_at']
            ])->first();

            if(!$pd) {
                $pd = new ProjectData();
                $pd->project_id = $project_id;
                $pd->session_id = $row['session_id'];
            }
            $pd->screen_data = $row['value'];
            $pd->date_time = $row['created_at'];
            $pd->save();
        }

        $requestTable = $db->query('SELECT * FROM RequestData');
        foreach($requestTable->fetchAll(mode: PDO::FETCH_ASSOC) as $row){
            $pd = ProjectData::query()->where([
                'session_id' => $row['session_id'],
                'project_id' => $project_id,
                'date_time' => $row['created_at']
            ])->first();

            if(!$pd) {
                $pd = new ProjectData();
                $pd->project_id = $project_id;
                $pd->session_id = $row['session_id'];
            }else{
                if($pd->request_method !== $row['method'] && $pd->request_url !== $row['url'])
                {
                    $pd = new ProjectData();
                    $pd->project_id = $project_id;
                    $pd->session_id = $row['session_id'];
                }
            }

            $pd->request_method = $row['method'];
            $pd->request_url = $row['url'];
            $pd->request_body = $row['body'];
            $pd->date_time = $row['created_at'];
            $pd->save();
        }

        $responseTable = $db->query('SELECT * FROM ResponseData');
        foreach($responseTable->fetchAll(mode: PDO::FETCH_ASSOC) as $row){
            $pd = ProjectData::query()->where([
                'session_id' => $row['session_id'],
                'project_id' => $project_id,
                'date_time' => $row['created_at']
            ])->first();

            if(!$pd) {
                $pd = new ProjectData();
                $pd->project_id = $project_id;
                $pd->session_id = $row['session_id'];
            }
            $pd->response_url = $row['url'];
            $pd->response_data = $row['value'];
            $pd->date_time = $row['created_at'];
            $pd->save();
        }

        if(Storage::exists($filename)){
            Storage::delete($filename);
        }else{
            dd('File does not exist.');
        }
    }
}
