<?php

namespace App\Http\Controllers\Api;

use App\Models\ProjectData;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\ProjectDataResource;
use App\Http\Resources\ProjectDataCollection;
use App\Http\Requests\ProjectDataStoreRequest;
use App\Http\Requests\ProjectDataUpdateRequest;

class ProjectDataController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', ProjectData::class);

        $search = $request->get('search', '');

        $allProjectData = ProjectData::search($search)
            ->latest()
            ->paginate();

        return new ProjectDataCollection($allProjectData);
    }

    /**
     * @param \App\Http\Requests\ProjectDataStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProjectDataStoreRequest $request)
    {
        $this->authorize('create', ProjectData::class);

        $validated = $request->validated();
        $validated['basic_data'] = json_decode($validated['basic_data'], true);

        $validated['basic_user'] = json_decode($validated['basic_user'], true);

        $validated['screen_data'] = json_decode(
            $validated['screen_data'],
            true
        );

        $validated['request_body'] = json_decode(
            $validated['request_body'],
            true
        );

        $validated['response_data'] = json_decode(
            $validated['response_data'],
            true
        );

        $projectData = ProjectData::create($validated);

        return new ProjectDataResource($projectData);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\ProjectData $projectData
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, ProjectData $projectData)
    {
        $this->authorize('view', $projectData);

        return new ProjectDataResource($projectData);
    }

    /**
     * @param \App\Http\Requests\ProjectDataUpdateRequest $request
     * @param \App\Models\ProjectData $projectData
     * @return \Illuminate\Http\Response
     */
    public function update(
        ProjectDataUpdateRequest $request,
        ProjectData $projectData
    ) {
        $this->authorize('update', $projectData);

        $validated = $request->validated();

        $validated['basic_data'] = json_decode($validated['basic_data'], true);

        $validated['basic_user'] = json_decode($validated['basic_user'], true);

        $validated['screen_data'] = json_decode(
            $validated['screen_data'],
            true
        );

        $validated['request_body'] = json_decode(
            $validated['request_body'],
            true
        );

        $validated['response_data'] = json_decode(
            $validated['response_data'],
            true
        );

        $projectData->update($validated);

        return new ProjectDataResource($projectData);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\ProjectData $projectData
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, ProjectData $projectData)
    {
        $this->authorize('delete', $projectData);

        $projectData->delete();

        return response()->noContent();
    }
}
