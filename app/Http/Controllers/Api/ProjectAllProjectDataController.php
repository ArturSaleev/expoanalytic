<?php

namespace App\Http\Controllers\Api;

use App\Models\Project;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\ProjectDataResource;
use App\Http\Resources\ProjectDataCollection;

class ProjectAllProjectDataController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Project $project
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Project $project)
    {
        $this->authorize('view', $project);

        $search = $request->get('search', '');

        $allProjectData = $project
            ->allProjectData()
            ->search($search)
            ->latest()
            ->paginate();

        return new ProjectDataCollection($allProjectData);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Project $project
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Project $project)
    {
        $this->authorize('create', ProjectData::class);

        $validated = $request->validate([
            'session_id' => ['required', 'max:255', 'string'],
            'basic_data' => ['nullable', 'max:255', 'json'],
            'basic_user' => ['nullable', 'max:255', 'json'],
            'visor_image' => ['nullable', 'max:255', 'string'],
            'screen_data' => ['nullable', 'max:255', 'json'],
            'request_method' => ['nullable', 'max:255', 'string'],
            'request_url' => ['nullable', 'max:255', 'string'],
            'request_body' => ['nullable', 'max:255', 'json'],
            'response_url' => ['nullable', 'max:255', 'string'],
            'response_data' => ['nullable', 'max:255', 'json'],
            'date_time' => ['required', 'date'],
        ]);

        $projectData = $project->allProjectData()->create($validated);

        return new ProjectDataResource($projectData);
    }
}
