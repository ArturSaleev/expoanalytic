<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\ProjectLoadFile;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Http\Resources\ProjectLoadFileResource;
use App\Http\Resources\ProjectLoadFileCollection;
use App\Http\Requests\ProjectLoadFileStoreRequest;
use App\Http\Requests\ProjectLoadFileUpdateRequest;

class ProjectLoadFileController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', ProjectLoadFile::class);

        $search = $request->get('search', '');

        $projectLoadFiles = ProjectLoadFile::search($search)
            ->latest()
            ->paginate();

        return new ProjectLoadFileCollection($projectLoadFiles);
    }

    /**
     * @param \App\Http\Requests\ProjectLoadFileStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProjectLoadFileStoreRequest $request)
    {
        $this->authorize('create', ProjectLoadFile::class);

        $validated = $request->validated();
        if ($request->hasFile('filename')) {
            $validated['filename'] = $request
                ->file('filename')
                ->store('public');
        }

        $projectLoadFile = ProjectLoadFile::create($validated);

        return new ProjectLoadFileResource($projectLoadFile);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\ProjectLoadFile $projectLoadFile
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, ProjectLoadFile $projectLoadFile)
    {
        $this->authorize('view', $projectLoadFile);

        return new ProjectLoadFileResource($projectLoadFile);
    }

    /**
     * @param \App\Http\Requests\ProjectLoadFileUpdateRequest $request
     * @param \App\Models\ProjectLoadFile $projectLoadFile
     * @return \Illuminate\Http\Response
     */
    public function update(
        ProjectLoadFileUpdateRequest $request,
        ProjectLoadFile $projectLoadFile
    ) {
        $this->authorize('update', $projectLoadFile);

        $validated = $request->validated();

        if ($request->hasFile('filename')) {
            if ($projectLoadFile->filename) {
                Storage::delete($projectLoadFile->filename);
            }

            $validated['filename'] = $request
                ->file('filename')
                ->store('public');
        }

        $projectLoadFile->update($validated);

        return new ProjectLoadFileResource($projectLoadFile);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\ProjectLoadFile $projectLoadFile
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, ProjectLoadFile $projectLoadFile)
    {
        $this->authorize('delete', $projectLoadFile);

        if ($projectLoadFile->filename) {
            Storage::delete($projectLoadFile->filename);
        }

        $projectLoadFile->delete();

        return response()->noContent();
    }
}
