<?php

namespace App\Http\Controllers\Api;

use App\Models\Project;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\ProjectMetaResource;
use App\Http\Resources\ProjectMetaCollection;

class ProjectProjectMetasController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Project $project
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Project $project)
    {
        $this->authorize('view', $project);

        $search = $request->get('search', '');

        $projectMetas = $project
            ->projectMetas()
            ->search($search)
            ->latest()
            ->paginate();

        return new ProjectMetaCollection($projectMetas);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Project $project
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Project $project)
    {
        $this->authorize('create', ProjectMeta::class);

        $validated = $request->validate([
            'name' => ['required', 'max:255', 'string'],
            'params' => ['required', 'max:255', 'string'],
            'chart_name' => ['required', 'max:255', 'string'],
            'on_main' => ['required', 'boolean'],
        ]);

        $projectMeta = $project->projectMetas()->create($validated);

        return new ProjectMetaResource($projectMeta);
    }
}
