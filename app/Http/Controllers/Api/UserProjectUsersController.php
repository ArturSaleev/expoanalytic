<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\ProjectUserResource;
use App\Http\Resources\ProjectUserCollection;

class UserProjectUsersController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, User $user)
    {
        $this->authorize('view', $user);

        $search = $request->get('search', '');

        $projectUsers = $user
            ->projectUsers()
            ->search($search)
            ->latest()
            ->paginate();

        return new ProjectUserCollection($projectUsers);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $user)
    {
        $this->authorize('create', ProjectUser::class);

        $validated = $request->validate([
            'project_id' => ['required', 'exists:projects,id'],
        ]);

        $projectUser = $user->projectUsers()->create($validated);

        return new ProjectUserResource($projectUser);
    }
}
