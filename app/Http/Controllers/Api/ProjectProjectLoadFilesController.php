<?php

namespace App\Http\Controllers\Api;

use App\Models\Project;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\ProjectLoadFileResource;
use App\Http\Resources\ProjectLoadFileCollection;

class ProjectProjectLoadFilesController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Project $project
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Project $project)
    {
        $this->authorize('view', $project);

        $search = $request->get('search', '');

        $projectLoadFiles = $project
            ->projectLoadFiles()
            ->search($search)
            ->latest()
            ->paginate();

        return new ProjectLoadFileCollection($projectLoadFiles);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Project $project
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Project $project)
    {
        $this->authorize('create', ProjectLoadFile::class);

        $validated = $request->validate([
            'filename' => ['nullable', 'file'],
        ]);

        if ($request->hasFile('filename')) {
            $validated['filename'] = $request
                ->file('filename')
                ->store('public');
        }

        $projectLoadFile = $project->projectLoadFiles()->create($validated);

        return new ProjectLoadFileResource($projectLoadFile);
    }
}
