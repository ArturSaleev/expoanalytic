<?php

namespace App\Http\Controllers\Api;

use App\Models\ProjectUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\ProjectUserResource;
use App\Http\Resources\ProjectUserCollection;
use App\Http\Requests\ProjectUserStoreRequest;
use App\Http\Requests\ProjectUserUpdateRequest;

class ProjectUserController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', ProjectUser::class);

        $search = $request->get('search', '');

        $projectUsers = ProjectUser::search($search)
            ->latest()
            ->paginate();

        return new ProjectUserCollection($projectUsers);
    }

    /**
     * @param \App\Http\Requests\ProjectUserStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProjectUserStoreRequest $request)
    {
        $this->authorize('create', ProjectUser::class);

        $validated = $request->validated();

        $projectUser = ProjectUser::create($validated);

        return new ProjectUserResource($projectUser);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\ProjectUser $projectUser
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, ProjectUser $projectUser)
    {
        $this->authorize('view', $projectUser);

        return new ProjectUserResource($projectUser);
    }

    /**
     * @param \App\Http\Requests\ProjectUserUpdateRequest $request
     * @param \App\Models\ProjectUser $projectUser
     * @return \Illuminate\Http\Response
     */
    public function update(
        ProjectUserUpdateRequest $request,
        ProjectUser $projectUser
    ) {
        $this->authorize('update', $projectUser);

        $validated = $request->validated();

        $projectUser->update($validated);

        return new ProjectUserResource($projectUser);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\ProjectUser $projectUser
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, ProjectUser $projectUser)
    {
        $this->authorize('delete', $projectUser);

        $projectUser->delete();

        return response()->noContent();
    }
}
