<?php

namespace App\Http\Controllers;

use App\Models\Project;
use Illuminate\Http\Request;
use App\Models\ProjectLoadFile;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\ProjectLoadFileStoreRequest;
use App\Http\Requests\ProjectLoadFileUpdateRequest;

class ProjectLoadFileController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', ProjectLoadFile::class);

        $search = $request->get('search', '');

        $projectLoadFiles = ProjectLoadFile::search($search)
            ->latest()
            ->paginate(5)
            ->withQueryString();

        return view(
            'app.project_load_files.index',
            compact('projectLoadFiles', 'search')
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', ProjectLoadFile::class);

        $projects = Project::pluck('name', 'id');

        return view('app.project_load_files.create', compact('projects'));
    }

    /**
     * @param \App\Http\Requests\ProjectLoadFileStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProjectLoadFileStoreRequest $request)
    {
        $this->authorize('create', ProjectLoadFile::class);

        $validated = $request->validated();
        if ($request->hasFile('filename')) {
            $validated['filename'] = $request
                ->file('filename')
                ->store('public');
        }

        $projectLoadFile = ProjectLoadFile::create($validated);

        return redirect()
            ->route('project-load-files.edit', $projectLoadFile)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\ProjectLoadFile $projectLoadFile
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, ProjectLoadFile $projectLoadFile)
    {
        $this->authorize('view', $projectLoadFile);

        return view('app.project_load_files.show', compact('projectLoadFile'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\ProjectLoadFile $projectLoadFile
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, ProjectLoadFile $projectLoadFile)
    {
        $this->authorize('update', $projectLoadFile);

        $projects = Project::pluck('name', 'id');

        return view(
            'app.project_load_files.edit',
            compact('projectLoadFile', 'projects')
        );
    }

    /**
     * @param \App\Http\Requests\ProjectLoadFileUpdateRequest $request
     * @param \App\Models\ProjectLoadFile $projectLoadFile
     * @return \Illuminate\Http\Response
     */
    public function update(
        ProjectLoadFileUpdateRequest $request,
        ProjectLoadFile $projectLoadFile
    ) {
        $this->authorize('update', $projectLoadFile);

        $validated = $request->validated();
        if ($request->hasFile('filename')) {
            if ($projectLoadFile->filename) {
                Storage::delete($projectLoadFile->filename);
            }

            $validated['filename'] = $request
                ->file('filename')
                ->store('public');
        }

        $projectLoadFile->update($validated);

        return redirect()
            ->route('project-load-files.edit', $projectLoadFile)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\ProjectLoadFile $projectLoadFile
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, ProjectLoadFile $projectLoadFile)
    {
        $this->authorize('delete', $projectLoadFile);

        if ($projectLoadFile->filename) {
            Storage::delete($projectLoadFile->filename);
        }

        $projectLoadFile->delete();

        return redirect()
            ->route('project-load-files.index')
            ->withSuccess(__('crud.common.removed'));
    }
}
