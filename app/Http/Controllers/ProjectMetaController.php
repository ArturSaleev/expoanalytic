<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\ProjectMeta;
use Illuminate\Http\Request;
use App\Http\Requests\ProjectMetaStoreRequest;
use App\Http\Requests\ProjectMetaUpdateRequest;

class ProjectMetaController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', ProjectMeta::class);

        $search = $request->get('search', '');

        $projectMetas = ProjectMeta::search($search)
            ->latest()
            ->paginate(5)
            ->withQueryString();

        return view(
            'app.project_metas.index',
            compact('projectMetas', 'search')
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', ProjectMeta::class);

        $projects = Project::pluck('name', 'id');

        return view('app.project_metas.create', compact('projects'));
    }

    /**
     * @param \App\Http\Requests\ProjectMetaStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProjectMetaStoreRequest $request)
    {
        $this->authorize('create', ProjectMeta::class);

        $validated = $request->validated();

        $projectMeta = ProjectMeta::create($validated);

        return redirect()
            ->route('project-metas.edit', $projectMeta)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\ProjectMeta $projectMeta
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, ProjectMeta $projectMeta)
    {
        $this->authorize('view', $projectMeta);

        return view('app.project_metas.show', compact('projectMeta'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\ProjectMeta $projectMeta
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, ProjectMeta $projectMeta)
    {
        $this->authorize('update', $projectMeta);

        $projects = Project::pluck('name', 'id');

        return view(
            'app.project_metas.edit',
            compact('projectMeta', 'projects')
        );
    }

    /**
     * @param \App\Http\Requests\ProjectMetaUpdateRequest $request
     * @param \App\Models\ProjectMeta $projectMeta
     * @return \Illuminate\Http\Response
     */
    public function update(
        ProjectMetaUpdateRequest $request,
        ProjectMeta $projectMeta
    ) {
        $this->authorize('update', $projectMeta);

        $validated = $request->validated();

        $projectMeta->update($validated);

        return redirect()
            ->route('project-metas.edit', $projectMeta)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\ProjectMeta $projectMeta
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, ProjectMeta $projectMeta)
    {
        $this->authorize('delete', $projectMeta);

        $projectMeta->delete();

        return redirect()
            ->route('project-metas.index')
            ->withSuccess(__('crud.common.removed'));
    }
}
