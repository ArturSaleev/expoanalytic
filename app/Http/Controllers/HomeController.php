<?php

namespace App\Http\Controllers;

use App\Helpers\MetaHelper;
use DateTime;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function index(Request $request): Renderable
    {
        $project = MetaHelper::ActiveProject();
        if($project === null){
            return view('home', ['result' => []]);
        }
        $result = $project->toArray();

        if($request->has('monthAndYear')){
            if($request->monthAndYear !== ""){
                $result['sessions'] = MetaHelper::SessionLast30Days($project->id);
            }else {
                $date_begin = date("Y-m-d", strtotime('01.' . $request->monthAndYear));
                $d = new DateTime($date_begin);
                $date_end = $d->format('Y-m-t');
                $result['sessions'] = MetaHelper::SessionLast30Days($project->id, $date_begin, $date_end);
            }
        }else {
            $result['sessions'] = MetaHelper::SessionLast30Days($project->id);
        }
        $result['month'] = MetaHelper::SessionMonthYear($project->id);

        return view('home', compact('result'));
    }

    public function setActiveProject($uid)
    {
        setcookie('activeProject', $uid);
        return redirect('home');
    }

    public function SessionsDate(Request $request)
    {
        $project = MetaHelper::ActiveProject();
        $data = MetaHelper::ListSessionsDate($project->id, $request->date);
        return view('sessions', ['result' => $data, "json" => json_encode($data)]);
    }
}
