<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProjectDataStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'project_id' => ['required', 'exists:projects,id'],
            'session_id' => ['required', 'max:255', 'string'],
            'basic_data' => ['nullable', 'max:255', 'json'],
            'basic_user' => ['nullable', 'max:255', 'json'],
            'visor_image' => ['nullable', 'max:255', 'string'],
            'screen_data' => ['nullable', 'max:255', 'json'],
            'request_method' => ['nullable', 'max:255', 'string'],
            'request_url' => ['nullable', 'max:255', 'string'],
            'request_body' => ['nullable', 'max:255', 'json'],
            'response_url' => ['nullable', 'max:255', 'string'],
            'response_data' => ['nullable', 'max:255', 'json'],
            'date_time' => ['required', 'date'],
        ];
    }
}
