<?php

namespace App\Policies;

use App\Models\User;
use App\Models\ProjectData;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProjectDataPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the projectData can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list allprojectdata');
    }

    /**
     * Determine whether the projectData can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\ProjectData  $model
     * @return mixed
     */
    public function view(User $user, ProjectData $model)
    {
        return $user->hasPermissionTo('view allprojectdata');
    }

    /**
     * Determine whether the projectData can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create allprojectdata');
    }

    /**
     * Determine whether the projectData can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\ProjectData  $model
     * @return mixed
     */
    public function update(User $user, ProjectData $model)
    {
        return $user->hasPermissionTo('update allprojectdata');
    }

    /**
     * Determine whether the projectData can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\ProjectData  $model
     * @return mixed
     */
    public function delete(User $user, ProjectData $model)
    {
        return $user->hasPermissionTo('delete allprojectdata');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\ProjectData  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete allprojectdata');
    }

    /**
     * Determine whether the projectData can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\ProjectData  $model
     * @return mixed
     */
    public function restore(User $user, ProjectData $model)
    {
        return false;
    }

    /**
     * Determine whether the projectData can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\ProjectData  $model
     * @return mixed
     */
    public function forceDelete(User $user, ProjectData $model)
    {
        return false;
    }
}
