<?php

namespace App\Policies;

use App\Models\User;
use App\Models\ProjectUser;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProjectUserPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the projectUser can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list projectusers');
    }

    /**
     * Determine whether the projectUser can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\ProjectUser  $model
     * @return mixed
     */
    public function view(User $user, ProjectUser $model)
    {
        return $user->hasPermissionTo('view projectusers');
    }

    /**
     * Determine whether the projectUser can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create projectusers');
    }

    /**
     * Determine whether the projectUser can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\ProjectUser  $model
     * @return mixed
     */
    public function update(User $user, ProjectUser $model)
    {
        return $user->hasPermissionTo('update projectusers');
    }

    /**
     * Determine whether the projectUser can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\ProjectUser  $model
     * @return mixed
     */
    public function delete(User $user, ProjectUser $model)
    {
        return $user->hasPermissionTo('delete projectusers');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\ProjectUser  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete projectusers');
    }

    /**
     * Determine whether the projectUser can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\ProjectUser  $model
     * @return mixed
     */
    public function restore(User $user, ProjectUser $model)
    {
        return false;
    }

    /**
     * Determine whether the projectUser can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\ProjectUser  $model
     * @return mixed
     */
    public function forceDelete(User $user, ProjectUser $model)
    {
        return false;
    }
}
