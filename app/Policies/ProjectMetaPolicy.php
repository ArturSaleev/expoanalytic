<?php

namespace App\Policies;

use App\Models\User;
use App\Models\ProjectMeta;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProjectMetaPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the projectMeta can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list projectmetas');
    }

    /**
     * Determine whether the projectMeta can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\ProjectMeta  $model
     * @return mixed
     */
    public function view(User $user, ProjectMeta $model)
    {
        return $user->hasPermissionTo('view projectmetas');
    }

    /**
     * Determine whether the projectMeta can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create projectmetas');
    }

    /**
     * Determine whether the projectMeta can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\ProjectMeta  $model
     * @return mixed
     */
    public function update(User $user, ProjectMeta $model)
    {
        return $user->hasPermissionTo('update projectmetas');
    }

    /**
     * Determine whether the projectMeta can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\ProjectMeta  $model
     * @return mixed
     */
    public function delete(User $user, ProjectMeta $model)
    {
        return $user->hasPermissionTo('delete projectmetas');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\ProjectMeta  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete projectmetas');
    }

    /**
     * Determine whether the projectMeta can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\ProjectMeta  $model
     * @return mixed
     */
    public function restore(User $user, ProjectMeta $model)
    {
        return false;
    }

    /**
     * Determine whether the projectMeta can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\ProjectMeta  $model
     * @return mixed
     */
    public function forceDelete(User $user, ProjectMeta $model)
    {
        return false;
    }
}
