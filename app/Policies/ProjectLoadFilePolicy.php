<?php

namespace App\Policies;

use App\Models\User;
use App\Models\ProjectLoadFile;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProjectLoadFilePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the projectLoadFile can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list projectloadfiles');
    }

    /**
     * Determine whether the projectLoadFile can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\ProjectLoadFile  $model
     * @return mixed
     */
    public function view(User $user, ProjectLoadFile $model)
    {
        return $user->hasPermissionTo('view projectloadfiles');
    }

    /**
     * Determine whether the projectLoadFile can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create projectloadfiles');
    }

    /**
     * Determine whether the projectLoadFile can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\ProjectLoadFile  $model
     * @return mixed
     */
    public function update(User $user, ProjectLoadFile $model)
    {
        return $user->hasPermissionTo('update projectloadfiles');
    }

    /**
     * Determine whether the projectLoadFile can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\ProjectLoadFile  $model
     * @return mixed
     */
    public function delete(User $user, ProjectLoadFile $model)
    {
        return $user->hasPermissionTo('delete projectloadfiles');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\ProjectLoadFile  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete projectloadfiles');
    }

    /**
     * Determine whether the projectLoadFile can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\ProjectLoadFile  $model
     * @return mixed
     */
    public function restore(User $user, ProjectLoadFile $model)
    {
        return false;
    }

    /**
     * Determine whether the projectLoadFile can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\ProjectLoadFile  $model
     * @return mixed
     */
    public function forceDelete(User $user, ProjectLoadFile $model)
    {
        return false;
    }
}
