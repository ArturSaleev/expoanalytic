<?php


namespace App\Helpers;


class DateHelper
{
    public static function MonthName($month)
    {
        $monthsArray = [
            "",
            "Январь",
            "Февраль",
            "Март",
            "Апрель",
            "Май",
            "Июнь",
            "Июль",
            "Август",
            "Сентябрь",
            "Октябрь",
            "Ноябрь",
            "Декабрь",
        ];

        return $monthsArray[$month] ?? "";
    }
}
