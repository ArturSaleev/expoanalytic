<?php


namespace App\Helpers;


use App\Models\Project;
use App\Models\ProjectData;
use App\Models\ProjectUser;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class MetaHelper
{
    public static function ActiveProject()
    {
        $projects = self::AllProject();
        if(empty($_COOKIE['activeProject'])){
            return $projects[0];
        }

        $uid = $_COOKIE['activeProject'];
        foreach($projects as $project){
            if($project->uid === $uid){
                return $project;
            }
        }
    }

    public static function AllProject()
    {
//        dd(Auth::id(), ProjectUser::all());
        $projectsId = ProjectUser::query()
//            ->where('user_id', Auth::id())
            ->pluck('project_id');

        return Project::query()->whereIn("id", $projectsId)->get(['id', 'uid', 'name']);
    }

    public static function SessionLast30Days($project_id, $date_begin = null, $date_end = null)
    {
        $dateSQL = "date_time > now() - interval '30 day'";
        if($date_begin !== null && $date_begin !== null){
            $dateSQL = "date_time between '$date_begin' and '$date_end'";
        }
        if($date_begin !== null && $date_begin == null){
            $dateSQL = "date_time > '$date_begin'";
        }
        if($date_begin == null && $date_begin !== null){
            $dateSQL = "date_time < '$date_begin'";
        }
        $sql = "select count(*) as cnt, query.date_time from (
                   select session_id,
                          to_char(date_time, 'dd.mm.yyyy') as date_time
                   from project_data
                   where $dateSQL
                   and project_id = $project_id
                   group by session_id, to_char(date_time, 'dd.mm.yyyy')
               ) query
                group by query.date_time
                order by query.date_time
                ";

        return DB::select($sql);
    }

    public static function SessionMonthYear($project_id)
    {
        $sql = "select month, year from (
                select EXTRACT('MONTH' FROM date_time) as month,
                       EXTRACT('YEAR' FROM date_time) as year
                   from project_data
                   where project_id = $project_id
                ) query
                group by query.month, query.year
                ";

        return DB::select($sql);
    }

    public static function ListSessionsDate($project_id, $date)
    {
        $sql = "select session_id,
               count(*) as cnt,
               to_char(min(date_time)::timestamptz, 'dd.mm.yyyy HH24:MI:SS') as start_date,
               to_char(max(date_time)::timestamptz, 'dd.mm.yyyy HH24:MI:SS') as end_date,
               to_char(min(date_time)::timestamptz, 'HH24:MI:SS') as start_time,
               to_char(max(date_time)::timestamptz, 'HH24:MI:SS') as end_time
        from project_data
        where project_id = $project_id
        and to_char(date_time, 'dd.mm.yyyy') = '$date'
        group by session_id
        order by min(date_time)";
        $pds =  DB::select($sql);

        $data = [];
        foreach($pds as $pd){
            $listsData = ProjectData::query()->where([
                "project_id" => $project_id,
                "session_id" => $pd->session_id
            ])->orderBy('date_time', 'asc')
                ->get();
            $pd->data = $listsData;

            $device = "";
            foreach($listsData as $listData){
                if($listData->basic_data !== null && $device == ''){
                    $basic_data = json_decode($listData->basic_data);
                    $device = $basic_data->deviceName;
                }
            }
            $pd->device = $device;
            array_push($data, $pd);
        }

        return $data;
    }

    public static function ProjectDevices($project_id)
    {
        $listsData = ProjectData::query()->where([
            "project_id" => $project_id
        ])
            ->orderBy('date_time', 'asc');
        foreach($listsData as $datum){
            dd($datum->basic_data);
        }
    }
}
