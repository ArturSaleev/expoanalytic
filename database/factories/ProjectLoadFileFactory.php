<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use App\Models\ProjectLoadFile;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProjectLoadFileFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ProjectLoadFile::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'project_id' => \App\Models\Project::factory(),
        ];
    }
}
