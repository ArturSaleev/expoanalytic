<?php

namespace Database\Factories;

use App\Models\ProjectMeta;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProjectMetaFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ProjectMeta::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'params' => $this->faker->text,
            'chart_name' => $this->faker->text(255),
            'on_main' => $this->faker->boolean,
            'project_id' => \App\Models\Project::factory(),
        ];
    }
}
