<?php

namespace Database\Factories;

use App\Models\ProjectData;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProjectDataFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ProjectData::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'session_id' => $this->faker->text(255),
            'basic_data' => [],
            'basic_user' => [],
            'visor_image' => $this->faker->text,
            'screen_data' => [],
            'request_method' => $this->faker->text(255),
            'request_url' => $this->faker->text(255),
            'request_body' => [],
            'response_url' => $this->faker->text(255),
            'response_data' => [],
            'date_time' => $this->faker->dateTime,
            'project_id' => \App\Models\Project::factory(),
        ];
    }
}
