<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_data', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('project_id');
            $table->string('session_id');
            $table->json('basic_data')->nullable();
            $table->json('basic_user')->nullable();
            $table->text('visor_image')->nullable();
            $table->json('screen_data')->nullable();
            $table->string('request_method')->nullable();
            $table->string('request_url')->nullable();
            $table->json('request_body')->nullable();
            $table->string('response_url')->nullable();
            $table->json('response_data')->nullable();
            $table->dateTime('date_time');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_data');
    }
};
