<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\PermissionRegistrar;

class PermissionsSeeder extends Seeder
{
    public function run()
    {
        // Reset cached roles and permissions
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        // Create default permissions
        Permission::create(['name' => 'list projects']);
        Permission::create(['name' => 'view projects']);
        Permission::create(['name' => 'create projects']);
        Permission::create(['name' => 'update projects']);
        Permission::create(['name' => 'delete projects']);

        Permission::create(['name' => 'list allprojectdata']);
        Permission::create(['name' => 'view allprojectdata']);
        Permission::create(['name' => 'create allprojectdata']);
        Permission::create(['name' => 'update allprojectdata']);
        Permission::create(['name' => 'delete allprojectdata']);

        Permission::create(['name' => 'list projectloadfiles']);
        Permission::create(['name' => 'view projectloadfiles']);
        Permission::create(['name' => 'create projectloadfiles']);
        Permission::create(['name' => 'update projectloadfiles']);
        Permission::create(['name' => 'delete projectloadfiles']);

        Permission::create(['name' => 'list projectmetas']);
        Permission::create(['name' => 'view projectmetas']);
        Permission::create(['name' => 'create projectmetas']);
        Permission::create(['name' => 'update projectmetas']);
        Permission::create(['name' => 'delete projectmetas']);

        Permission::create(['name' => 'list projectusers']);
        Permission::create(['name' => 'view projectusers']);
        Permission::create(['name' => 'create projectusers']);
        Permission::create(['name' => 'update projectusers']);
        Permission::create(['name' => 'delete projectusers']);

        // Create user role and assign existing permissions
        $currentPermissions = Permission::all();
        $userRole = Role::create(['name' => 'user']);
        $userRole->givePermissionTo($currentPermissions);

        // Create admin exclusive permissions
        Permission::create(['name' => 'list roles']);
        Permission::create(['name' => 'view roles']);
        Permission::create(['name' => 'create roles']);
        Permission::create(['name' => 'update roles']);
        Permission::create(['name' => 'delete roles']);

        Permission::create(['name' => 'list permissions']);
        Permission::create(['name' => 'view permissions']);
        Permission::create(['name' => 'create permissions']);
        Permission::create(['name' => 'update permissions']);
        Permission::create(['name' => 'delete permissions']);

        Permission::create(['name' => 'list users']);
        Permission::create(['name' => 'view users']);
        Permission::create(['name' => 'create users']);
        Permission::create(['name' => 'update users']);
        Permission::create(['name' => 'delete users']);

        // Create admin role and assign all permissions
        $allPermissions = Permission::all();
        $adminRole = Role::create(['name' => 'super-admin']);
        $adminRole->givePermissionTo($allPermissions);

        $user = \App\Models\User::whereEmail('admin@admin.com')->first();

        if ($user) {
            $user->assignRole($adminRole);
        }
    }
}
