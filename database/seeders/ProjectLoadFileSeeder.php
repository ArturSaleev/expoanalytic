<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ProjectLoadFile;

class ProjectLoadFileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProjectLoadFile::factory()
            ->count(5)
            ->create();
    }
}
