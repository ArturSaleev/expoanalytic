<?php

namespace Database\Seeders;

use App\Models\ProjectData;
use Illuminate\Database\Seeder;

class ProjectDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProjectData::factory()
            ->count(5)
            ->create();
    }
}
