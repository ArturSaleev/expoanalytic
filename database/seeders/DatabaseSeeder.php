<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Adding an admin user
        $user = \App\Models\User::factory()
            ->count(1)
            ->create([
                'email' => 'admin@webeasy.kz',
                'password' => \Hash::make('admin'),
            ]);
        $this->call(PermissionsSeeder::class);

//        $this->call(ProjectSeeder::class);
//        $this->call(ProjectDataSeeder::class);
//        $this->call(ProjectLoadFileSeeder::class);
//        $this->call(ProjectMetaSeeder::class);
//        $this->call(ProjectUserSeeder::class);
//        $this->call(UserSeeder::class);
    }
}
