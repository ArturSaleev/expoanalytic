<?php

namespace Database\Seeders;

use App\Models\ProjectMeta;
use Illuminate\Database\Seeder;

class ProjectMetaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProjectMeta::factory()
            ->count(5)
            ->create();
    }
}
