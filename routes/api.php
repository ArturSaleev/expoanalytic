<?php

use App\Http\Controllers\Api\UploadLite;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\RoleController;
use App\Http\Controllers\Api\ProjectController;
use App\Http\Controllers\Api\PermissionController;
use App\Http\Controllers\Api\ProjectUserController;
use App\Http\Controllers\Api\ProjectDataController;
use App\Http\Controllers\Api\ProjectMetaController;
use App\Http\Controllers\Api\ProjectLoadFileController;
use App\Http\Controllers\Api\UserProjectUsersController;
use App\Http\Controllers\Api\ProjectProjectUsersController;
use App\Http\Controllers\Api\ProjectProjectMetasController;
use App\Http\Controllers\Api\ProjectAllProjectDataController;
use App\Http\Controllers\Api\ProjectProjectLoadFilesController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/uploadSQLite', [UploadLite::class, 'load']);
Route::post('/login', [AuthController::class, 'login'])->name('api.login');

Route::middleware('auth:sanctum')
    ->get('/user', function (Request $request) {
        return $request->user();
    })
    ->name('api.user');

Route::name('api.')
    ->middleware('auth:sanctum')
    ->group(function () {
        Route::apiResource('roles', RoleController::class);
        Route::apiResource('permissions', PermissionController::class);

        Route::apiResource('users', UserController::class);

        // User Project Users
        Route::get('/users/{user}/project-users', [
            UserProjectUsersController::class,
            'index',
        ])->name('users.project-users.index');
        Route::post('/users/{user}/project-users', [
            UserProjectUsersController::class,
            'store',
        ])->name('users.project-users.store');

        Route::apiResource('project-users', ProjectUserController::class);

        Route::apiResource(
            'project-load-files',
            ProjectLoadFileController::class
        );

        Route::apiResource('all-project-data', ProjectDataController::class);

        Route::apiResource('projects', ProjectController::class);

        // Project Project Users
        Route::get('/projects/{project}/project-users', [
            ProjectProjectUsersController::class,
            'index',
        ])->name('projects.project-users.index');
        Route::post('/projects/{project}/project-users', [
            ProjectProjectUsersController::class,
            'store',
        ])->name('projects.project-users.store');

        // Project Project Load Files
        Route::get('/projects/{project}/project-load-files', [
            ProjectProjectLoadFilesController::class,
            'index',
        ])->name('projects.project-load-files.index');
        Route::post('/projects/{project}/project-load-files', [
            ProjectProjectLoadFilesController::class,
            'store',
        ])->name('projects.project-load-files.store');

        // Project All Project Data
        Route::get('/projects/{project}/all-project-data', [
            ProjectAllProjectDataController::class,
            'index',
        ])->name('projects.all-project-data.index');
        Route::post('/projects/{project}/all-project-data', [
            ProjectAllProjectDataController::class,
            'store',
        ])->name('projects.all-project-data.store');

        // Project Project Metas
        Route::get('/projects/{project}/project-metas', [
            ProjectProjectMetasController::class,
            'index',
        ])->name('projects.project-metas.index');
        Route::post('/projects/{project}/project-metas', [
            ProjectProjectMetasController::class,
            'store',
        ])->name('projects.project-metas.store');

        Route::apiResource('project-metas', ProjectMetaController::class);
    });
