<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\ProjectUserController;
use App\Http\Controllers\ProjectDataController;
use App\Http\Controllers\ProjectMetaController;
use App\Http\Controllers\ProjectLoadFileController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::get('/home/{uid}', [HomeController::class, 'setActiveProject']);
Route::get('/sessions', [HomeController::class, 'SessionsDate']);

Route::prefix('/')
    ->middleware('auth')
    ->group(function () {
        Route::resource('roles', RoleController::class);
        Route::resource('permissions', PermissionController::class);

        Route::resource('users', UserController::class);
        Route::resource('project-users', ProjectUserController::class);
        Route::resource('project-load-files', ProjectLoadFileController::class);
        Route::get('all-project-data', [
            ProjectDataController::class,
            'index',
        ])->name('all-project-data.index');
        Route::post('all-project-data', [
            ProjectDataController::class,
            'store',
        ])->name('all-project-data.store');
        Route::get('all-project-data/create', [
            ProjectDataController::class,
            'create',
        ])->name('all-project-data.create');
        Route::get('all-project-data/{projectData}', [
            ProjectDataController::class,
            'show',
        ])->name('all-project-data.show');
        Route::get('all-project-data/{projectData}/edit', [
            ProjectDataController::class,
            'edit',
        ])->name('all-project-data.edit');
        Route::put('all-project-data/{projectData}', [
            ProjectDataController::class,
            'update',
        ])->name('all-project-data.update');
        Route::delete('all-project-data/{projectData}', [
            ProjectDataController::class,
            'destroy',
        ])->name('all-project-data.destroy');

        Route::resource('projects', ProjectController::class);
        Route::resource('project-metas', ProjectMetaController::class);
    });
