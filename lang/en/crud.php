<?php

return [
    'common' => [
        'actions' => 'Actions',
        'create' => 'Create',
        'edit' => 'Edit',
        'update' => 'Update',
        'new' => 'New',
        'cancel' => 'Cancel',
        'attach' => 'Attach',
        'detach' => 'Detach',
        'save' => 'Save',
        'delete' => 'Delete',
        'delete_selected' => 'Delete selected',
        'search' => 'Search...',
        'back' => 'Back to Index',
        'are_you_sure' => 'Are you sure?',
        'no_items_found' => 'No items found',
        'created' => 'Successfully created',
        'saved' => 'Saved successfully',
        'removed' => 'Successfully removed',
    ],

    'users' => [
        'name' => 'Users',
        'index_title' => 'Users List',
        'new_title' => 'New User',
        'create_title' => 'Create User',
        'edit_title' => 'Edit User',
        'show_title' => 'Show User',
        'inputs' => [
            'name' => 'Name',
            'email' => 'Email',
            'password' => 'Password',
        ],
    ],

    'project_users' => [
        'name' => 'Project Users',
        'index_title' => 'ProjectUsers List',
        'new_title' => 'New Project user',
        'create_title' => 'Create ProjectUser',
        'edit_title' => 'Edit ProjectUser',
        'show_title' => 'Show ProjectUser',
        'inputs' => [
            'user_id' => 'User',
            'project_id' => 'Project',
        ],
    ],

    'project_load_files' => [
        'name' => 'Project Load Files',
        'index_title' => 'ProjectLoadFiles List',
        'new_title' => 'New Project load file',
        'create_title' => 'Create ProjectLoadFile',
        'edit_title' => 'Edit ProjectLoadFile',
        'show_title' => 'Show ProjectLoadFile',
        'inputs' => [
            'project_id' => 'Project',
            'filename' => 'Filename',
        ],
    ],

    'all_project_data' => [
        'name' => 'All Project Data',
        'index_title' => 'AllProjectData List',
        'new_title' => 'New Project data',
        'create_title' => 'Create ProjectData',
        'edit_title' => 'Edit ProjectData',
        'show_title' => 'Show ProjectData',
        'inputs' => [
            'project_id' => 'Project',
            'session_id' => 'Session Id',
            'basic_data' => 'Basic Data',
            'basic_user' => 'Basic User',
            'visor_image' => 'Visor Image',
            'screen_data' => 'Screen Data',
            'request_method' => 'Request Method',
            'request_url' => 'Request Url',
            'request_body' => 'Request Body',
            'response_url' => 'Response Url',
            'response_data' => 'Response Data',
            'date_time' => 'Date Time',
        ],
    ],

    'projects' => [
        'name' => 'Projects',
        'index_title' => 'Projects List',
        'new_title' => 'New Project',
        'create_title' => 'Create Project',
        'edit_title' => 'Edit Project',
        'show_title' => 'Show Project',
        'inputs' => [
            'uid' => 'Uid',
            'name' => 'Name',
        ],
    ],

    'project_metas' => [
        'name' => 'Project Metas',
        'index_title' => 'ProjectMetas List',
        'new_title' => 'New Project meta',
        'create_title' => 'Create ProjectMeta',
        'edit_title' => 'Edit ProjectMeta',
        'show_title' => 'Show ProjectMeta',
        'inputs' => [
            'project_id' => 'Project',
            'name' => 'Name',
            'params' => 'Params',
            'chart_name' => 'Chart Name',
            'on_main' => 'On Main',
        ],
    ],

    'roles' => [
        'name' => 'Roles',
        'index_title' => 'Roles List',
        'create_title' => 'Create Role',
        'edit_title' => 'Edit Role',
        'show_title' => 'Show Role',
        'inputs' => [
            'name' => 'Name',
        ],
    ],

    'permissions' => [
        'name' => 'Permissions',
        'index_title' => 'Permissions List',
        'create_title' => 'Create Permission',
        'edit_title' => 'Edit Permission',
        'show_title' => 'Show Permission',
        'inputs' => [
            'name' => 'Name',
        ],
    ],
];
