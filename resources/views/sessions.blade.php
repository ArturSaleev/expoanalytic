@extends('layouts.app')

@section('content')
    <?php
    $startData = $result[0];
    $firstData = $startData->data[0];
    $mainImages = "";
    foreach ($startData->data as $data) {
        if (($mainImages == "") && ($data->visor_image !== "")) {
            $mainImages = $data->visor_image;
        }
    }
    ?>
    <div class="container">
        <div class="row">

            <div class="col-md-3">
                <div class="card">
                    <div class="card-header">
                        Time Line Sessions Visor
                    </div>
                    <div class="card-body">
                        <img src="{!! $mainImages !!}" id="visor_image" style="width: 100%">
                    </div>
                    <div class="card-footer" style="text-align: center">
                        <div class="btn-group">
                            <button type="button" class="btn btn-default" id="btn_left" disabled="true"
                                    onclick="setNext(false)">
                                <i class="icon ion-ios-arrow-back"></i>
                            </button>
                            <span class="btn btn-default" id="count_list">1 из {{ count($startData->data) }}</span>
                            <button type="button" class="btn btn-default" id="btn_right" onclick="setNext(true)">
                                <i class="icon ion-ios-arrow-forward"></i>
                            </button>
                        </div>
                    </div>
                    <label id="date_time2" style="margin: 10px;text-align: center;"></label>
                </div>
            </div>

            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <select class="form-control" id="selectSession" onchange="clickSessionSelect()">
                            @foreach($result as $i=>$res)
                                <option value="{{ $i }}">{{ $i+1 }} - {{ $res->device }} ({{ $res->start_time }} - {{ $res->end_time }})</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <label class="col-lg-4">
                                Активность (с - по)
                            </label>
                            <div class="col-lg-8" id="period_date">
                                {{ $result[0]->start_date }} - {{ $result[0]->end_date }}
                            </div>
                        </div>

                        <div class="row">
                            <label class="col-lg-4">
                                Страница
                            </label>
                            <div class="col-lg-8">
                                <textarea id="screen_data" class="form-control">
                                    {!! $firstData->screen_data !!}
                                </textarea>
                            </div>
                        </div>

                        <div class="row">
                            <label class="col-lg-4">URL</label>
                            <div class="col-lg-8" id="request_url"></div>
                        </div>
                        <div class="row">
                            <label class="col-lg-4">Запрос</label>
                            <div class="col-lg-8">
                                <textarea id="request_body" class="form-control"></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-lg-4">Ответ</label>
                            <div class="col-lg-8">
                                <textarea id="response_body" class="form-control"></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-lg-4">Basic Data</label>
                            <div class="col-lg-8">
                                <textarea id="basic_data" class="form-control"></textarea>
                            </div>
                        </div>

                        <div class="row">
                            <label class="col-lg-4">Дата и время</label>
                            <div class="col-lg-8">
                                <label id="date_time"></label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="card">
                    <div class="card-body">
                        <img src="" style="width: 100%; border-radius: 50%" id="user_photo">
                        <br>
                        <div style="margin-top: 10px; text-align: right">
                            <hr/>
                            <label id="user_login"></label><br>
                            <label id="user_lastName"></label><br>
                            <label id="user_firstName"></label><br>
                            <label id="user_secondName"></label><br>
                            <hr/>

                            <div class="row">
                                <label class="col-lg-6">Device</label>
                                <label class="col-lg-6" id="deviceName"></label>
                            </div>
                            <div class="row">
                                <label class="col-lg-6">isDevice</label>
                                <label class="col-lg-6" id="isDevice"></label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        let startNext = 0;
        let result = {!! $json !!};
        let activeResult = result[0];
        let activeData = result[0].data;

        function clickSessionSelect() {
            let id = $('#selectSession').val();
            activeResult = result[id];
            activeData = result[id].data;

            startNext = 0;
            showSessionParams();
            getActiveData(startNext);
            $('#btn_left').attr('disabled', true);
            $('#btn_right').attr('disabled', false);

        }

        function showSessionParams() {
            $('#period_date').html(activeResult.start_date + ' - ' + activeResult.end_date);
            let userData = {};
            let basicData = {};
            let img = "";
            activeData.map(e => {
                if (img === '' && e.visor_image !== '') {
                    img = e.visor_image;
                    $('#visor_image').attr('src', e.visor_image);
                }

                if (e.basic_user !== null) {
                    let user = JSON.parse(e.basic_user);
                    if (Object.keys(user).length > 0) {
                        userData = user;
                    }

                    let base = JSON.parse(e.basic_data)
                    if (Object.keys(base).length > 0) {
                        basicData = base;
                    }
                }
            })

            $('#user_photo').attr('src', userData.photo);
            $('#user_login').html(userData.login);
            $('#user_lastName').html(userData.lastName);
            $('#user_firstName').html(userData.firstName);
            $('#user_secondName').html(userData.secondName);
        }

        function setNext(next = true) {
            if (next === true) {
                startNext++;
            } else {
                startNext--;
            }
            getActiveData(startNext);
            $('#btn_left').attr('disabled', (startNext === 0));
            $('#btn_right').attr('disabled', (startNext + 1 === activeData.length));
        }

        function getActiveData(id = 0) {
            $('#screen_data').val(activeData[id].screen_data);
            $('#request_url').html(activeData[id].request_method + ': ' + activeData[id].request_url);
            $('#request_body').val(activeData[id].request_body);
            $('#response_body').val(activeData[id].response_body);
            $('#basic_data').val(activeData[id].basic_data);
            if (activeData[id].visor_image !== null) {
                $('#visor_image').attr('src', activeData[id].visor_image)
            }
            $('#date_time').html(activeData[id].date_time);
            $('#date_time2').html(activeData[id].date_time);
            let ids = id + 1;
            $('#count_list').html(ids + ' из ' + activeData.length)

            let data = JSON.parse(activeData[id].basic_data);
            if (data !== null) {
                $('#deviceName').html(data.deviceName);
                $('#isDevice').html((data.isDevice) ? 'Device' : 'Emulator');
            }
        }

        showSessionParams();
        getActiveData();

    </script>
@endpush
