@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">
                <a href="{{ route('project-metas.index') }}" class="mr-4"
                    ><i class="icon ion-md-arrow-back"></i
                ></a>
                @lang('crud.project_metas.show_title')
            </h4>

            <div class="mt-4">
                <div class="mb-4">
                    <h5>@lang('crud.project_metas.inputs.project_id')</h5>
                    <span
                        >{{ optional($projectMeta->project)->name ?? '-'
                        }}</span
                    >
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.project_metas.inputs.name')</h5>
                    <span>{{ $projectMeta->name ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.project_metas.inputs.params')</h5>
                    <span>{{ $projectMeta->params ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.project_metas.inputs.chart_name')</h5>
                    <span>{{ $projectMeta->chart_name ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.project_metas.inputs.on_main')</h5>
                    <span>{{ $projectMeta->on_main ?? '-' }}</span>
                </div>
            </div>

            <div class="mt-4">
                <a
                    href="{{ route('project-metas.index') }}"
                    class="btn btn-light"
                >
                    <i class="icon ion-md-return-left"></i>
                    @lang('crud.common.back')
                </a>

                @can('create', App\Models\ProjectMeta::class)
                <a
                    href="{{ route('project-metas.create') }}"
                    class="btn btn-light"
                >
                    <i class="icon ion-md-add"></i> @lang('crud.common.create')
                </a>
                @endcan
            </div>
        </div>
    </div>
</div>
@endsection
