@php $editing = isset($projectMeta) @endphp

<div class="row">
    <x-inputs.group class="col-sm-12">
        <x-inputs.select name="project_id" label="Project" required>
            @php $selected = old('project_id', ($editing ? $projectMeta->project_id : '')) @endphp
            <option disabled {{ empty($selected) ? 'selected' : '' }}>Please select the Project</option>
            @foreach($projects as $value => $label)
            <option value="{{ $value }}" {{ $selected == $value ? 'selected' : '' }} >{{ $label }}</option>
            @endforeach
        </x-inputs.select>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.text
            name="name"
            label="Name"
            value="{{ old('name', ($editing ? $projectMeta->name : '')) }}"
            maxlength="255"
            placeholder="Name"
            required
        ></x-inputs.text>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.textarea name="params" label="Params" maxlength="255" required
            >{{ old('params', ($editing ? $projectMeta->params : ''))
            }}</x-inputs.textarea
        >
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.text
            name="chart_name"
            label="Chart Name"
            value="{{ old('chart_name', ($editing ? $projectMeta->chart_name : '')) }}"
            maxlength="255"
            placeholder="Chart Name"
            required
        ></x-inputs.text>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.checkbox
            name="on_main"
            label="On Main"
            :checked="old('on_main', ($editing ? $projectMeta->on_main : 0))"
        ></x-inputs.checkbox>
    </x-inputs.group>
</div>
