@php $editing = isset($projectData) @endphp

<div class="row">
    <x-inputs.group class="col-sm-12">
        <x-inputs.select name="project_id" label="Project" required>
            @php $selected = old('project_id', ($editing ? $projectData->project_id : '')) @endphp
            <option disabled {{ empty($selected) ? 'selected' : '' }}>Please select the Project</option>
            @foreach($projects as $value => $label)
            <option value="{{ $value }}" {{ $selected == $value ? 'selected' : '' }} >{{ $label }}</option>
            @endforeach
        </x-inputs.select>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.text
            name="session_id"
            label="Session Id"
            value="{{ old('session_id', ($editing ? $projectData->session_id : '')) }}"
            maxlength="255"
            placeholder="Session Id"
            required
        ></x-inputs.text>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.textarea name="basic_data" label="Basic Data" maxlength="255"
            >{{ old('basic_data', ($editing ?
            json_encode($projectData->basic_data) : '')) }}</x-inputs.textarea
        >
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.textarea name="basic_user" label="Basic User" maxlength="255"
            >{{ old('basic_user', ($editing ?
            json_encode($projectData->basic_user) : '')) }}</x-inputs.textarea
        >
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.textarea
            name="visor_image"
            label="Visor Image"
            maxlength="255"
            >{{ old('visor_image', ($editing ? $projectData->visor_image : ''))
            }}</x-inputs.textarea
        >
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.textarea
            name="screen_data"
            label="Screen Data"
            maxlength="255"
            >{{ old('screen_data', ($editing ?
            json_encode($projectData->screen_data) : '')) }}</x-inputs.textarea
        >
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.text
            name="request_method"
            label="Request Method"
            value="{{ old('request_method', ($editing ? $projectData->request_method : '')) }}"
            maxlength="255"
            placeholder="Request Method"
        ></x-inputs.text>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.text
            name="request_url"
            label="Request Url"
            value="{{ old('request_url', ($editing ? $projectData->request_url : '')) }}"
            maxlength="255"
            placeholder="Request Url"
        ></x-inputs.text>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.textarea
            name="request_body"
            label="Request Body"
            maxlength="255"
            >{{ old('request_body', ($editing ?
            json_encode($projectData->request_body) : '')) }}</x-inputs.textarea
        >
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.text
            name="response_url"
            label="Response Url"
            value="{{ old('response_url', ($editing ? $projectData->response_url : '')) }}"
            maxlength="255"
            placeholder="Response Url"
        ></x-inputs.text>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.textarea
            name="response_data"
            label="Response Data"
            maxlength="255"
            >{{ old('response_data', ($editing ?
            json_encode($projectData->response_data) : ''))
            }}</x-inputs.textarea
        >
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.datetime
            name="date_time"
            label="Date Time"
            value="{{ old('date_time', ($editing ? optional($projectData->date_time)->format('Y-m-d\TH:i:s') : '')) }}"
            max="255"
            required
        ></x-inputs.datetime>
    </x-inputs.group>
</div>
