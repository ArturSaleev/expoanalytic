@extends('layouts.app')

@section('content')
<div class="container">
    <div class="searchbar mt-0 mb-4">
        <div class="row">
            <div class="col-md-6">
                <form>
                    <div class="input-group">
                        <input
                            id="indexSearch"
                            type="text"
                            name="search"
                            placeholder="{{ __('crud.common.search') }}"
                            value="{{ $search ?? '' }}"
                            class="form-control"
                            autocomplete="off"
                        />
                        <div class="input-group-append">
                            <button type="submit" class="btn btn-primary">
                                <i class="icon ion-md-search"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-6 text-right">
                @can('create', App\Models\ProjectData::class)
                <a
                    href="{{ route('all-project-data.create') }}"
                    class="btn btn-primary"
                >
                    <i class="icon ion-md-add"></i> @lang('crud.common.create')
                </a>
                @endcan
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <div style="display: flex; justify-content: space-between;">
                <h4 class="card-title">
                    @lang('crud.all_project_data.index_title')
                </h4>
            </div>

            <div class="table-responsive">
                <table class="table table-borderless table-hover">
                    <thead>
                        <tr>
                            <th class="text-left">
                                @lang('crud.all_project_data.inputs.project_id')
                            </th>
                            <th class="text-left">
                                @lang('crud.all_project_data.inputs.session_id')
                            </th>
{{--                            <th class="text-left">--}}
{{--                                @lang('crud.all_project_data.inputs.basic_data')--}}
{{--                            </th>--}}
{{--                            <th class="text-left">--}}
{{--                                @lang('crud.all_project_data.inputs.basic_user')--}}
{{--                            </th>--}}
{{--                            <th class="text-left">--}}
{{--                                @lang('crud.all_project_data.inputs.visor_image')--}}
{{--                            </th>--}}
{{--                            <th class="text-left">--}}
{{--                                @lang('crud.all_project_data.inputs.screen_data')--}}
{{--                            </th>--}}
{{--                            <th class="text-left">--}}
{{--                                @lang('crud.all_project_data.inputs.request_method')--}}
{{--                            </th>--}}
{{--                            <th class="text-left">--}}
{{--                                @lang('crud.all_project_data.inputs.request_url')--}}
{{--                            </th>--}}
{{--                            <th class="text-left">--}}
{{--                                @lang('crud.all_project_data.inputs.request_body')--}}
{{--                            </th>--}}
{{--                            <th class="text-left">--}}
{{--                                @lang('crud.all_project_data.inputs.response_url')--}}
{{--                            </th>--}}
{{--                            <th class="text-left">--}}
{{--                                @lang('crud.all_project_data.inputs.response_data')--}}
{{--                            </th>--}}
                            <th class="text-left">
                                @lang('crud.all_project_data.inputs.date_time')
                            </th>
                            <th class="text-center">
                                @lang('crud.common.actions')
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($allProjectData as $projectData)
                        <tr>
                            <td>
                                {{ optional($projectData->project)->name ?? '-'
                                }}
                            </td>
                            <td>{{ $projectData->session_id ?? '-' }}</td>
{{--                            <td>--}}
{{--                                <pre>--}}
{{--{{ json_encode($projectData->basic_data) ?? '-' }}</pre--}}
{{--                                >--}}
{{--                            </td>--}}
{{--                            <td>--}}
{{--                                <pre>--}}
{{--{{ json_encode($projectData->basic_user) ?? '-' }}</pre--}}
{{--                                >--}}
{{--                            </td>--}}
{{--                            <td>{{ $projectData->visor_image ?? '-' }}</td>--}}
{{--                            <td>--}}
{{--                                <pre>--}}
{{--{{ json_encode($projectData->screen_data) ?? '-' }}</pre--}}
{{--                                >--}}
{{--                            </td>--}}
{{--                            <td>{{ $projectData->request_method ?? '-' }}</td>--}}
{{--                            <td>{{ $projectData->request_url ?? '-' }}</td>--}}
{{--                            <td>--}}
{{--                                <pre>--}}
{{--{{ json_encode($projectData->request_body) ?? '-' }}</pre--}}
{{--                                >--}}
{{--                            </td>--}}
{{--                            <td>{{ $projectData->response_url ?? '-' }}</td>--}}
{{--                            <td>--}}
{{--                                <pre>--}}
{{--{{ json_encode($projectData->response_data) ?? '-' }}</pre--}}
{{--                                >--}}
{{--                            </td>--}}
                            <td>{{ $projectData->date_time ?? '-' }}</td>
                            <td class="text-center" style="width: 134px;">
                                <div
                                    role="group"
                                    aria-label="Row Actions"
                                    class="btn-group"
                                >
                                    @can('update', $projectData)
                                    <a
                                        href="{{ route('all-project-data.edit', $projectData) }}"
                                    >
                                        <button
                                            type="button"
                                            class="btn btn-light"
                                        >
                                            <i class="icon ion-md-create"></i>
                                        </button>
                                    </a>
                                    @endcan @can('view', $projectData)
                                    <a
                                        href="{{ route('all-project-data.show', $projectData) }}"
                                    >
                                        <button
                                            type="button"
                                            class="btn btn-light"
                                        >
                                            <i class="icon ion-md-eye"></i>
                                        </button>
                                    </a>
                                    @endcan @can('delete', $projectData)
                                    <form
                                        action="{{ route('all-project-data.destroy', $projectData) }}"
                                        method="POST"
                                        onsubmit="return confirm('{{ __('crud.common.are_you_sure') }}')"
                                    >
                                        @csrf @method('DELETE')
                                        <button
                                            type="submit"
                                            class="btn btn-light text-danger"
                                        >
                                            <i class="icon ion-md-trash"></i>
                                        </button>
                                    </form>
                                    @endcan
                                </div>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="13">
                                @lang('crud.common.no_items_found')
                            </td>
                        </tr>
                        @endforelse
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="13">
                                {!! $allProjectData->render() !!}
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
