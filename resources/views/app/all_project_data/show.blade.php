@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">
                <a href="{{ route('all-project-data.index') }}" class="mr-4"
                    ><i class="icon ion-md-arrow-back"></i
                ></a>
                @lang('crud.all_project_data.show_title')
            </h4>

            <div class="mt-4">
                <div class="mb-4">
                    <h5>@lang('crud.all_project_data.inputs.project_id')</h5>
                    <span
                        >{{ optional($projectData->project)->name ?? '-'
                        }}</span
                    >
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.all_project_data.inputs.session_id')</h5>
                    <span>{{ $projectData->session_id ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.all_project_data.inputs.basic_data')</h5>
                    <pre>
{{ json_encode($projectData->basic_data) ?? '-' }}</pre
                    >
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.all_project_data.inputs.basic_user')</h5>
                    <pre>
{{ json_encode($projectData->basic_user) ?? '-' }}</pre
                    >
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.all_project_data.inputs.visor_image')</h5>
                    @if($projectData->visor_image)
                        <img src="{{$projectData->visor_image}}" />
                    @endif
{{--                    <span>{{ $projectData->visor_image ?? '-' }}</span>--}}
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.all_project_data.inputs.screen_data')</h5>
                    <pre>
{{ json_encode($projectData->screen_data) ?? '-' }}</pre
                    >
                </div>
                <div class="mb-4">
                    <h5>
                        @lang('crud.all_project_data.inputs.request_method')
                    </h5>
                    <span>{{ $projectData->request_method ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.all_project_data.inputs.request_url')</h5>
                    <span>{{ $projectData->request_url ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.all_project_data.inputs.request_body')</h5>
                    <pre>
{{ json_encode($projectData->request_body) ?? '-' }}</pre
                    >
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.all_project_data.inputs.response_url')</h5>
                    <span>{{ $projectData->response_url ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.all_project_data.inputs.response_data')</h5>
                    <pre>
{{ json_encode($projectData->response_data) ?? '-' }}</pre
                    >
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.all_project_data.inputs.date_time')</h5>
                    <span>{{ $projectData->date_time ?? '-' }}</span>
                </div>
            </div>

            <div class="mt-4">
                <a
                    href="{{ route('all-project-data.index') }}"
                    class="btn btn-light"
                >
                    <i class="icon ion-md-return-left"></i>
                    @lang('crud.common.back')
                </a>

                @can('create', App\Models\ProjectData::class)
                <a
                    href="{{ route('all-project-data.create') }}"
                    class="btn btn-light"
                >
                    <i class="icon ion-md-add"></i> @lang('crud.common.create')
                </a>
                @endcan
            </div>
        </div>
    </div>
</div>
@endsection
