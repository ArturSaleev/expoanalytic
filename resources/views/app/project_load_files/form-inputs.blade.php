@php $editing = isset($projectLoadFile) @endphp

<div class="row">
    <x-inputs.group class="col-sm-12">
        <x-inputs.select name="project_id" label="Project" required>
            @php $selected = old('project_id', ($editing ? $projectLoadFile->project_id : '')) @endphp
            <option disabled {{ empty($selected) ? 'selected' : '' }}>Please select the Project</option>
            @foreach($projects as $value => $label)
            <option value="{{ $value }}" {{ $selected == $value ? 'selected' : '' }} >{{ $label }}</option>
            @endforeach
        </x-inputs.select>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.partials.label
            name="filename"
            label="Filename"
        ></x-inputs.partials.label
        ><br />

        <input
            type="file"
            name="filename"
            id="filename"
            class="form-control-file"
        />

        @if($editing && $projectLoadFile->filename)
        <div class="mt-2">
            <a
                href="{{ \Storage::url($projectLoadFile->filename) }}"
                target="_blank"
                ><i class="icon ion-md-download"></i>&nbsp;Download</a
            >
        </div>
        @endif @error('filename') @include('components.inputs.partials.error')
        @enderror
    </x-inputs.group>
</div>
