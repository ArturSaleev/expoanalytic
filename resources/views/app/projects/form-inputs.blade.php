<?php
    use Illuminate\Support\Str;
?>
@php $editing = isset($project) @endphp

<div class="row">
    <x-inputs.group class="col-sm-12">
        <x-inputs.hidden
            name="uid"
{{--            label="Uid"--}}
            value="{{ old('uid', ($editing ? $project->uid : Str::uuid())) }}"
            maxlength="255"
            placeholder="Uid"
            required
        ></x-inputs.hidden>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.text
            name="name"
            label="Name"
            value="{{ old('name', ($editing ? $project->name : '')) }}"
            maxlength="255"
            placeholder="Name"
            required
        ></x-inputs.text>
    </x-inputs.group>
</div>
