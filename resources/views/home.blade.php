@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div id="chart_{{ $result['uid'] }}"></div>
                    </div>
                    <div class="card-footer row">
                        <label class="col-md-4">Фильтр по месяцам</label>
                        <div class="col-md-8">
                            <form id="monthAndYearForm">
                                <select class="form-control" name="monthAndYear"
                                        onchange="$('#monthAndYearForm').submit();">
                                    <option value="">---</option>
                                    @foreach($result['month'] as $month)
                                        <?php
                                            $selected = (app('request')->input('monthAndYear') == $month->month . "." . $month->year) ? 'selected' : '';
                                        ?>
                                        <option value="{{ $month->month.".".$month->year }}" {{ $selected }}>
                                            {{ \App\Helpers\DateHelper::MonthName($month->month) }} {{ $month->year }} г.
                                        </option>
                                    @endforeach
                                </select>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        Список активных сессии за последние 30 дней
                    </div>
                    {{--                    <div class="card-body" >--}}
                    <table class="table table-bordered card-body" style="height: 355px; overflow: auto">
                        @foreach($result['sessions'] as $res)
                            <tr>
                                <td><a href="{{ asset('sessions?date='.$res->date_time) }}">{{ $res->date_time }}</a>
                                </td>
                                <td>{{ $res->cnt }}</td>
                            </tr>
                        @endforeach
                    </table>
                    {{--                    </div>--}}
                </div>
            </div>

        </div>
    </div>
@endsection

@push('styles')
    <script src="{{ asset('js/apexcharts.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/promise-polyfill@8/dist/polyfill.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/eligrey-classlist-js-polyfill@1.2.20171210/classList.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/findindex_polyfill_mdn"></script>
@endpush

@push('scripts')
    <script>
        <?php
        $data = [];
        $categories = [];
        foreach ($result['sessions'] as $ses) {
            array_push($data, $ses->cnt);
            array_push($categories, $ses->date_time);
        }
        ?>
        $(document).ready(function () {
            var chart_{{ $result['id'] }} = new ApexCharts(document.querySelector("#chart_{{ $result['uid'] }}"), {
                series: [{
                    name: "Кол-во",
                    data: {!! json_encode($data) !!} //[10, 41, 35, 51, 49, 62, 69, 91, 148]
                }],
                chart: {
                    height: 350,
                    type: 'line',
                    zoom: {
                        enabled: false
                    }
                },
                dataLabels: {
                    enabled: false
                },
                stroke: {
                    curve: 'straight'
                },
                title: {
                    text: 'Активные сессии за последние 30 дней',
                    align: 'center'
                },
                grid: {
                    row: {
                        colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
                        opacity: 0.5
                    },
                },
                xaxis: {
                    categories: {!! json_encode($categories) !!}, //['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep'],
                }
            });
            chart_{{ $result['id'] }}.render();
        });

    </script>
@endpush
